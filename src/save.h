#ifndef SAVE_H
#define SAVE_H

// =====   HEADER FILE INCLUDES   =====

#include <random> // std::mt19937 and std::uniform_int_distribution
#include "pedigree.h"
#include "abc.h"

// ====   FUNCTION'S  PROTOTYPES   ====

void output_header ( const Settings& );
void save_nnlr ( const Pedigree& , const std::vector<Nectar>& , const std::vector<Nectar>& , const Settings& );
void save_model (name , bool , const std::vector<Nectar>& , coefficient , coefficient , coefficient , const Pedigree& , const Settings& );
void print_summary ( const Settings& , const Pedigree& , coefficient , coefficient , const Vector& , coefficient , coefficient );
void save_log ( const Settings& , name );
void save_database ( Pedigree& , const Settings& , coefficient );
coefficient bootstrap_pvalue ( const Settings& , const Pedigree& , coefficient , coefficient , coefficient , coefficient , Vector , Matrix );

// ====   INLINE FUNCTIONS   ====

/* =======================================================================================
 * Function panalysis ()
 * ---------------------------------------------------------------------------------------
 * Prints the kind of analysis performed.
 ======================================================================================= */
inline name panalysis (Model model) {
	return (tr_model(model)+" model");
}

/* =======================================================================================
 * Function pmodel ()
 * ---------------------------------------------------------------------------------------
 * Prints the model used.
 ======================================================================================= */
inline name pmodel (Model model) {
	if      (model == IP) return "IP";
	else if (model == BA) return "BA";
	else if (model == BW) return "BW";
	else if (model == MX) return "MX";
	else return "No purging";
}

/* =======================================================================================
 * Function pmethod ()
 * ---------------------------------------------------------------------------------------
 * Prints the method used.
 ======================================================================================= */
inline name pmethod (Method method) {
	if (method == NNLR) return "NNLR";
	else return "";
}

/* =======================================================================================
 * Function pcomment ()
 * ---------------------------------------------------------------------------------------
 * Returns a comment from a sentence
 ======================================================================================= */
inline name pcomment (name comment) {
	return "# " + comment + "\n";
}

#endif
