/* =======================================================================================
 *                                                                                       *
 *       Filename:  save.cpp                                                             *
 *                                                                                       *
 *    Description:  This file contains functions to save analysis an results.            *
 *                                                                                       *
 *                  -> output_header ()                                                  *
 *                  -> save_nnlr ()                                                      *
 *                  -> print_summary ()                                                  *
 *                  -> save_log ()                                                       *
 *                  -> save_database ()                                                  *
 *                  -> bootstrap_pvalue ()                                               *
 *                                                                                       *
 ======================================================================================= */

#include "save.h"


/* =======================================================================================
 * Function output_header ()
 * ---------------------------------------------------------------------------------------
 * Outputs the header for NNLR output files.
 ======================================================================================= */
void output_header (const Settings& settings) {

	/* SETTINGS VARIABLES */
	Model model (settings.get_model());
	sVector fnames (settings.get_fnames());
	sVector nnames (settings.get_nnames());
	sMatrix categories (settings.get_categories());
	bool maternal (settings.get_use_maternal());

	/* LOCAL VARIABLES */
	name fitness (",W0");
	name slope ("b(g)");
	name fa_names ("");
	name slope_ffa ("b(F·Fa)");
	name slope_fa ("b(Fa)");
	name sloped ("b(gdam)");
	name extra ("");

	if (settings.is_w0_noinbred()) fitness += ",SE(W0)";
	if (model != IP) {
		slope = "b(F)";
		sloped = "b(Fdam)";
	}
	if (model == BA || model == MX) fa_names += ',' + slope_ffa;
	if (model == BW || model == MX ) fa_names += ',' + slope_fa;
	if (maternal) extra += ',' + sloped;
	for (auto const& i: fnames)	extra += ",b(" + i + ')';
	size_t start (0);
	if (settings.is_w0_b()) start = 1;
	for (size_t i(0); i<nnames.size(); ++i) {
		for (size_t j(start); j<categories[i].size(); ++j) {
			extra += ",b(" + nnames[i] + ':' + categories[i][j] + ')';
		}
	}

	/* OUTPUT FILE NAME TAG */
	MM_FLAG = "_ip";
	if      (model == BA) MM_FLAG = "_ffa";
	else if (model == BW) MM_FLAG = "_fa";
	else if (model == MX) MM_FLAG = "_faffa";

	name output_file (FILE_NAME + MM_FLAG + CSV);
	std::ofstream output (output_file);

	/* OUTPUT HEADERS */
	output << "Analysis,d coefficient,RSS,AICc,RL,Chi2,p-value (Chi2),p-value (bootstrap)";
	output << fitness << ',' << slope << fa_names << extra << std::endl;

}

/* =======================================================================================
 * Function save_nnlr ()
 * ---------------------------------------------------------------------------------------
 * Its saves output for NNLR method
 ======================================================================================= */
void save_nnlr (const Pedigree& pedigree, const std::vector<Nectar>& pantry, const std::vector<Nectar>& null_pantry, const Settings& settings) {

	/* GENERATE OUTPUT HEADER */
	output_header (settings);

	/* LOCAL VARIABLES */
	name file (pedigree.get_name());
	Model model (settings.get_model());
	Vector fitness (pedigree.get_fitness());
	size_t n (fitness.size());

	// If N runs > 1, then an average solution is calculated.
	Nectar result (pantry[0]), null_result (null_pantry[0]);
	Nectar sd_result, sd_null_result;
	if (pantry.size() > 1) {
		result = nectar_mean(pantry); sd_result = nectar_sd(pantry,result);
		null_result = nectar_mean(null_pantry);	sd_null_result = nectar_sd(null_pantry,null_result);
	}

	/* STATISTIC CALCULATION */
	size_t k (result.factors.size()); // number of estimated factors, including slope on g (or F)
	if (settings.is_w0_b()) ++k;
	if (!settings.is_delta_num()) ++k;
	coefficient AICc ( AIC( result.stats.RSS, n, k, true, model));
	if (pantry.size()>1) AICc = nectar_mean_AIC (pantry, n, k, model);
	coefficient null_AICc (AIC(null_result.stats.RSS,n,k,true,ID));
	if (pantry.size()>1) null_AICc = nectar_mean_AIC (null_pantry,n,k,ID);
	coefficient boot_pval (1.0);
	boot_pval = bootstrap_pvalue(settings, pedigree, result.d, result.w0.value, result.delta.value, null_result.delta.value, get_parameter_values(result.reg_coefficients), result.factors);

	/* WRITE OUTPUT (model) */
	name output_file (FILE_NAME + MM_FLAG + CSV);
	std::ofstream output (output_file, std::ios_base::app);
	save_model (output_file, true, pantry, AICc, null_AICc, boot_pval, pedigree, settings);
	save_model (output_file, false, null_pantry, null_AICc, AICc, boot_pval, pedigree, settings);

	/* PRINT SUMMARY IN VERBOSE MODE */
	if (VERBOSE) {
		coefficient print_delta (result.delta.value);
		if (settings.is_delta_nopurged()) print_delta = null_result.delta.value;
		print_summary (settings, pedigree, result.d, print_delta, get_parameter_values(result.reg_coefficients), AICc, boot_pval);
	}

}

/* =======================================================================================
 * Function save_model ()
 * ---------------------------------------------------------------------------------------
 * Saves one line of data for the corresponding purging model
 ======================================================================================= */
void save_model (name filename, bool purging, const std::vector<Nectar>& pantry, coefficient ref_AIC, coefficient alt_AIC, coefficient boot_pval, const Pedigree& pedigree, const Settings& settings) {

	/* READ MAIN RESULTS */
	Nectar result (pantry[0]);
	Nectar sd_result;
	if (pantry.size() > 1) {
		result = nectar_mean(pantry);
		sd_result = nectar_sd(pantry, result);
	}

	/* STATISTIC CALCULATION */
	size_t k (result.factors.size()); // number of estimated factors, including slope on g (or F)
	if (settings.is_w0_b()) ++k;
	if (!settings.is_delta_num()) ++k;
	size_t n (pedigree.get_fitness().size());

	/* OUTPUT */
	std::ofstream output (filename, std::ios_base::app);
	// model
	if (purging) output << panalysis(settings.get_model()) << ',';
	else output << panalysis(ID) << ',';
	// purging coefficient
	output << result.d << ',';
	// RSS, AIC
	output << result.stats.RSS << ',' << ref_AIC << ',';
	// RL
	output << RL_AIC({ref_AIC, alt_AIC}, ref_AIC) << ',';
	// CHI2, pval(CHI2), pval(bootstrap)
	if (purging) output << CHI2 (ref_AIC, alt_AIC) << ',' << p_valCHI2 (ref_AIC, alt_AIC) << ',' << boot_pval << ',';
	else output << ",,,";
	// W0
	output << result.w0.value << ',';
	if (settings.is_w0_noinbred()) output << pedigree.get_w0().ET << ',';
	// delta
	output << -result.delta.value;
	// additional (numerical) factors
	if (!purging) {
		if (settings.get_fa_model()) output << ',';
		if (settings.get_model()==MX) output << ',';
	}
	for (const auto& i:result.reg_coefficients) {
		output << ',' << i.value;
	}
	output << std::endl;

	/* STANDARD ERRORS */
	if (settings.get_nruns() > 1) {
		coefficient sd_AIC (nectar_sd_AIC(pantry, ref_AIC, n, k, settings.get_model()));
		output << "SD"           << ','
			<< sd_result.d    << ','
			<< sd_result.stats.RSS << ','
			<< sd_AIC << ','
			<< ','
			<< ','
			<< ','
			<< ',';
			output << sd_result.w0.value << ',';
		if (settings.is_w0_noinbred()) output << 0.0	<< ',';
			output << sd_result.delta.value;
		if (!purging) {
			if (settings.get_fa_model()) output << ',';
			if (settings.get_model()==MX) output << ',';
		}
		for (const auto& i:sd_result.reg_coefficients) {
			output << ',' << i.value;
		}
		output << std::endl;
	}

}

/* =======================================================================================
 * Function print summary ()
 * ---------------------------------------------------------------------------------------
 * Prints a short summary in the terminal with the main purging parameter estimates.
 ======================================================================================= */
void print_summary (const Settings& settings, const Pedigree& pedigree, coefficient d, coefficient delta, const Vector& factors, coefficient AIC, coefficient pval) {

	/* LOCAL VARIABLES */
	Model model (settings.get_model());
	name file (pedigree.get_name());

	/* BEST RESULTS ARE PRINTED */
	pequal(pe,'=');
	if (model == IP) {
		std::cout << "The purging coefficient that best fit the data [" << file << "] is: \n";
		std::cout << "\t* d = " << d << " ( bootstrap p-value = "  << pval << "; AICc = " << AIC << " )\n";
	} else if (settings.is_delta_nopurged()) {
		std::cout << "The estimated inbreeding depression rate using Fa=0 data [" << file << "] is: \n";
		std::cout << "\t* delta = " << -delta << " ( AICc = " << AIC << " )\n";
	} else if (model != IP && model !=ID) {
		std::cout << "The purging term in " << tr_model (model) << " model that best fit the data [" << file << "] is:\n";
		if (model != BW) std::cout << "\t* bFFa = " << factors[0] << " ( bootstrap p-value = " << pval << "; AICc = " << AIC << " )\n";
		else             std::cout << "\t* bFa = "  << factors[0] << " ( bootstrap p-value = " << pval << "; AICc = " << AIC << " )\n";
		if (model == MX) std::cout << "\t* bFa = "  << factors[1] << "\n";
	}

}

/* =======================================================================================
 * Function save_log ()
 * ---------------------------------------------------------------------------------------
 * Saves a log file indicating the actual settings used in the analysis.
 ======================================================================================= */
void save_log (const Settings& settings, name filename) {

	/* LOCAL VARIABLES */
	iVector factors (settings.get_factors());
	sVector fnames (settings.get_fnames())  ;
	iVector nominal (settings.get_nominal());
	sVector nnames (settings.get_nnames());
	name log_filename (settings.get_log())  ;
	if (log_filename.size()) {
		log_filename = "\"" + log_filename + "\"";
	} else {
		log_filename = "1";
	}

	/* CURRENT TIME AND DATE */
	time_t current_time (time(0));
    struct tm * date = localtime( & current_time );

	/* SAVE LOG */
	name logfile (FILE_NAME+"_log"+CSV);
	if (settings.get_log().size()) {
		logfile = settings.get_log();
	}

	std::ofstream output (logfile);
	output << "# PURGd v2.2 log for " << filename << CSV << " ("
		<< date->tm_hour        << ':' <<
		   date->tm_min         << ':' <<
		   date->tm_sec         << ' '
		<< date->tm_mday        << '/' <<
	       date->tm_mon + 1     << '/' <<
	       date->tm_year + 1900 << ")\n\n";

	output << pcomment("How to set up PURGd:");
	output << pcomment("A short description and a list of possible valid values for each available");
	output << pcomment("option is given below. Modify these options by entering a value after the");
	output << pcomment("equal sign [=] next to the name. If a field is left blank, PURGd will use");
	output << pcomment("the corresponding default value for that option. Note that these values can");
	output << pcomment("be overriden if the program is run with command-line arguments.");
	output << std::endl;
	output << pcomment("Use the command-line option --config in order to run PURGd using the settings");
	output << pcomment("specified in this file.");
	output << std::endl;
	output << pcomment("Refer to the user's guide for an in-depth explanation about PURGd options.");
	output << std::endl;
	output << pcomment("PURGd OPTIONS ---------------------------------------------------------------");
	output << std::endl;
	output << pcomment("General options -------------------------------------------------------------");
	output << std::endl;
	output << pcomment("Seed");
	output << pcomment("Value to generate pseudorandom numbers during the analysis.");
	output << pcomment("A blank value means to use the current time.");
	output << pcomment("Allowed: integer                                              Default:");
	output << std::endl;
	output << "SEED=" << settings.get_seed() << std::endl;
	output << std::endl;
	output << pcomment("Verbose mode");
	output << pcomment("Print a short summary of the results in the terminal.");
	output << pcomment("Allowed: 0 (OFF), 1 (ON)                                      Default: 0");
	output << std::endl;
	output << "VERBOSE=" << VERBOSE << std::endl;
	output << std::endl;
	output << pcomment("Accuracy");
	output << pcomment("Accuracy value to obtain the estimate of the purging coefficient.");
	output << pcomment("Note: This option only applies to the IP model.");
	output << pcomment("Allowed: number (up to 0.5)                                   Default: 0.01");
	output << std::endl;
	output << "ACCURACY=" << settings.get_accuracy() << std::endl;
	output << std::endl;
	output << pcomment("Model options ---------------------------------------------------------------");
	output << std::endl;
	output << pcomment("Model");
	output << pcomment("The model used to predict the expected value of the fitness trait:");
	output << pcomment("inbreeding-purging [IP], Ballou's [BA], Boakes & Wang's [BW], or Ballou and");
	output << pcomment("Boakes & Wang's mixed model [MX].");
	output << pcomment("Allowed: IP, BA, BW, MX                                       Default: IP");
	output << std::endl;
	output << "MODEL=" << pmodel(settings.get_model()) << std::endl;
	output << std::endl;
	output << pcomment("Analysis options ------------------------------------------------------------");
	output << std::endl;
	output << pcomment("Gene dropping simulation");
	output << pcomment("Number of iterations to run the gene dropping simulation.");
	output << pcomment("If left blank, gene dropping is not used and expected ancestral inbreeding");
	output << pcomment("values are computed using Ballou's equation.");
	output << pcomment("Allowed: integer (gene dropping), blank (expected values)     Default:");
	output << std::endl;
	output << "GENEDROP=";
	if (settings.get_genedrop()) output << settings.get_n_genedrop();
	output << std::endl;
	output << std::endl;
	output << pcomment("Fixed purging coefficient");
	output << pcomment("A fixed value of the purging coefficient can be set, instead of estimating it");
	output << pcomment("Allowed: number                                               Default:");
	output << std::endl;
	output << "D=";
	if (settings.is_d_fixed()) output << settings.get_fixed_d();
	output << std::endl;
	output << std::endl;
	output << pcomment("Initial average fitness");
	output << pcomment("Initial average fitness of the population.");
	output << pcomment("It is estimated during the analysis as the average fitness of non-inbred individuals");
	output << pcomment("with non-inbred ancestors [noinbred]. Alternatively, it can set to a given value, or");
	output << pcomment("estimated as any other parameter in the model (leaving the space below empty).");
	output << pcomment("Allowed: number, noinbred                                     Default: noinbred");
	output << std::endl;
	output << "W0=";
	if (settings.is_w0_num()) output << settings.get_w0_num();
	else if (settings.is_w0_noinbred()) output << "noinbred";
	output << std::endl;
	output << std::endl;
	output << pcomment("Inbreeding load");
	output << pcomment("Inbreeding load of the population.");
	output << pcomment("By default, the inbreeding load is estimated at the same time as the remaining");
	output << pcomment("parameters of the model. It can also be estimated from individuals without ancestral");
	output << pcomment("inbreeding [nopurged], or set to a given value.");
	output << pcomment("Allowed: number, nopurged                                     Default:");
	output << std::endl;
	output << "BG=";
	if (settings.is_delta_num()) output << settings.get_delta_num();
	else if (settings.is_delta_nopurged()) output << "nopurged";
	output << std::endl;
	output << std::endl;
	output << pcomment("Use maternal effects");
	output << pcomment("Introduce maternal effects as an additional factor in the analysis.");
	output << pcomment("Allowed: 0 (OFF), 1 (ON)                                      Default: 0");
	output << std::endl;
	output << "MATERNAL=" << settings.get_use_maternal() << std::endl;
	output << std::endl;
	output << pcomment("Additional numeric factors");
	output << pcomment("Number(s) of the column(s) containing the additional factor(s) in the input");
	output << pcomment("file, separated by commas (e.g.: 5,6).");
	output << pcomment("These factors must be numeric (i.e. quantitative measures).");
	output << pcomment("Allowed: integer                                              Default:");
	output << std::endl;
	output << "ADD_FACTORS=";
	for (size_t i(0); i<factors.size(); ++i) {
		if (i) {
			output << ',';
		}	output << factors[i]+1;
	}; output << std::endl;
	output << std::endl;
	output << pcomment("Names of the additional numeric factors");
	output << pcomment("Name(s) for the additional factor(s) in the input file, separated by commas");
	output << pcomment("(e.g.: YEAR_OF_BIRTH).");
	output << pcomment("Allowed: string                                               Default:");
	output << std::endl;
	output << "NAME_FACTORS=";
	for (size_t i(0); i<fnames.size(); ++i)  {
		if (i) {
			output << ',';
		}	output << fnames[i];
	}; output << std::endl;
	output << std::endl;
	output << pcomment("Additional categorical factors");
	output << pcomment("Number(s) of the column(s) containing the additional factor(s) in the input");
	output << pcomment("file, separated by commas (e.g.: 7,8).");
	output << pcomment("These factors will be tretated as categorical variables (i.e. qualitative");
	output << pcomment("measures).");
	output << pcomment("Allowed: integer                                              Default:");
	output << std::endl;
	output << "ADD_CFACTORS=";
	for (size_t i(0); i<nominal.size(); ++i) {
		if (i) {
			output << ',';
		}	output << nominal[i]+1;
	}; output << std::endl;
	output << std::endl;
	output << pcomment("Names of additional categorical factors");
	output << pcomment("Name(s) for the additional factor(s) in the input file, separated by commas");
	output << pcomment("(e.g.: LOCATION).");
	output << pcomment("Allowed: string                                               Default:");
	output << std::endl;
	output << "NAME_CFACTORS=";
	for (size_t i(0); i<nnames.size(); ++i)  {
		if (i) {
			output << ',';
		}	output << nnames[i];
	}; output << std::endl;
	output << std::endl;
	output << pcomment("Output options ---------------------------------------------------------");
	output << std::endl;
	output << pcomment("Save log");
	output << pcomment("Save a log file containing the settings used in the analysis.");
	output << pcomment("Logs may have a custom file name.");
	output << pcomment("Allowed: 0 (OFF), 1 (ON, default name), string (ON, custom)   Default: 0");
	output << std::endl;
	output << "SAVE_LOG=" << log_filename << std::endl;
	output << std::endl;
	output << pcomment("Save databases");
	output << pcomment("Save a database containing both the fitness and the inbreeding coefficients");
	output << pcomment("of the analysed (or all) individuals.");
	output << pcomment("Allowed: 0 (OFF), 1 (ON), ALL (ON for all individuals)        Default: 0");
	output << std::endl;
	std::string log_db ("0");
	if (settings.get_save_db_all()) log_db = "ALL";
	else if (settings.get_save_db()) log_db = "1";
	output << "SAVE_DB=" << log_db << std::endl;
	output << std::endl;
	output << pcomment("Number of bees");
	output << pcomment("Number of solutions explored by iteration.");
	output << pcomment("Allowed: integer                                              Default: 250");
	output << std::endl;
	output << pcomment("NNLR options -----------------------------------------------------------");
	output << std::endl;
	output << "NBEES=" << settings.get_nbees () << std::endl;
	output << std::endl;
	output << pcomment("Limit for convergence");
	output << pcomment("Required number of consecutive iterations without improving results.");
	output << pcomment("Allowed: integer                                              Default: 10");
	output << std::endl;
	output << "LIMIT=" << settings.get_limit () << std::endl;
	output << std::endl;
	output << pcomment("Number of runs");
	output << pcomment("Number of times to run the ABC algorithm. Results will be averaged.");
	output << pcomment("Allowed: integer                                              Default: 1");
	output << std::endl;
	output << "NRUNS=" << settings.get_nruns() << std::endl;
	output << std::endl;
	output << pcomment("Maximum value of initial fitness");
	output << pcomment("Maximum value to explore for the initial average fitness.");
	output << pcomment("Allowed: number                                               Default: 1.0");
	output << std::endl;
	output << "MAX_W0=" << settings.get_max_w0() << std::endl;
	output << std::endl;
	output << pcomment("Maximum value of the inbreeding depression rate");
	output << pcomment("Maximum value to explore for the inbreeding depression rate.");
	output << pcomment("Allowed: number                                               Default: 10");
	output << std::endl;
	output << "MAX_BG=" << settings.get_max_delta()	<< std::endl;
	output << std::endl;
	output << pcomment("Minimum and maximum values for other regression terms");
	output << pcomment("Minimum and maximum values to explore for the effects of additional factors,");
	output << pcomment("separated by commas.");
	output << pcomment("Allowed: number,number                                        Default: -10,10");
	output << std::endl;
	output << "RANGE_FACTORS=" << settings.get_range_factors().min << "," << settings.get_range_factors().max			<< std::endl;
	output << std::endl;
}

/* =======================================================================================
 * Function save_database ()
 * ---------------------------------------------------------------------------------------
 * Saves a database for a single pedigree file containing parameters as IDs, fitness and
 * inbreeding coefficients. This file only include evaluated individuals.
 ======================================================================================= */
void save_database (Pedigree& pedigree, const Settings& settings, coefficient d) {

	/* LOCAL VARIABLES */
	name file (pedigree.get_name());
	bool maternal (settings.get_use_maternal());
	DB db (pedigree.read_db());
	bool use_genedrop (settings.get_genedrop());
	bool save_all (settings.get_save_db_all());

	Vector g (pedigree.calc_g(d));
	Vector gd;
	if (maternal) gd = pedigree.calc_mat(g);
	bVector eval (pedigree.get_eval());
	if (save_all) eval = bVector(eval.size(), true);
	sVector fnames (settings.get_fnames());
	sVector nnames (settings.get_nnames());
	iVector findex (settings.get_factors());
	iVector cindex (settings.get_nominal());

	/* OPEN DATABASE */
	name dbname (file+"_data"+CSV);
	std::ofstream database (dbname);

	/* HEADER */
	name db_w ("W");
	database << "ID,"
			 << "W";
	database << ",F"
			 << ",g(" << d << ")"
			 //<< ",Accumulated purge"
			 << ",Fa";
	if (use_genedrop) database << ",Fa(genedrop)";
	if (maternal) database << ",gdam(" << d << ")";
	for (const auto& name: fnames) database << ',' << name;
	for (const auto& name: nnames) database << ',' << name;
	database << std::endl;

	/* BODY */
	std::size_t N (db.ids.size());
	for (size_t i(0); i<N; ++i) {
		if (eval[i]) {
			//coefficient accumulated_purge(0.0);
			//if (F[j] > 0.0) accumulated_purge = 1.0 - g[j] / F[j];

			database << db.ids[i] << ','
					 << db.w[i] << ','
					 << db.F[i] << ','
					 << g[i] << ','
					 //<< accumulated_purge << ','
					 << db.Fa[i];
			if (use_genedrop) database << ',' << db.Fag[i];
			if (maternal) database << ',' << gd[i];
			for (size_t k(0); k<findex.size(); ++k) database << ',' << db.factors[i][findex[k]-4];
			for (size_t k(0); k<cindex.size(); ++k)	database << ',' << db.factors[i][cindex[k]-4];
			database << std::endl;
		}
	}
}

/* =======================================================================================
 * bootstrap_pvalue ()
 * ---------------------------------------------------------------------------------------
 * Calculates a bootstrap p-value for measuring the statistical significance of the purging
 * parameter estimates tested against the null hypothesis d=0.
 ======================================================================================= */
coefficient bootstrap_pvalue (const Settings& settings, const Pedigree& pedigree, coefficient d, coefficient w0, coefficient delta, coefficient null_delta, Vector v_factors, Matrix m_factors) {

	/* LOCAL VARIABLES */
	Model model (settings.get_model());
	Method method (settings.get_method());
	coefficient accuracy (settings.get_accuracy());
	bool fa_model (settings.get_fa_model());

	Vector fitness (pedigree.get_fitness());
	Vector f (pedigree.get_F());
	Vector fa (pedigree.get_Fa());
	Vector g (*pedigree.get_g()[0]);
	if (pedigree.get_g().size() > 1) g = (*pedigree.get_g()[d/accuracy]);
	size_t n (fitness.size());
	size_t cont (0); //the number of fa-based factors
	if (fa_model) ++cont;
	if (model==MX) ++cont;

	Vector Xi,Yi;
	coefficient MD (0.0);
	Vector fitness_d; // vector of expected values of fitness under the purging hypohtesis
	Vector fitness_d0; // vector of expected values of fitness under the non-purging hypohtesis
	coefficient pvalue (0.0);

	/* EXPECTED FITNESS CALCULATIONS */
	for (size_t i(0); i<n; ++i) {

		coefficient add_factors (0.0); // the expected contribution of maternal effects and other additional factors to fitness		
		if (v_factors.size() > cont) {
			for (size_t j(cont); j<v_factors.size(); ++j) {
				add_factors += v_factors[j] * m_factors[i][j];
			}
		}

		if (method == NNLR) {
			if      (model == IP) fitness_d.push_back (w0 * exp(-delta * g[i]                                                    + add_factors));
			else if (model == BA) fitness_d.push_back (w0 * exp(-delta * f[i] + v_factors[0] * f[i]*fa[i]                        + add_factors));
			else if (model == BW) fitness_d.push_back (w0 * exp(-delta * f[i]                             + v_factors[0] * fa[i] + add_factors));
			else if (model == MX) fitness_d.push_back (w0 * exp(-delta * f[i] + v_factors[0] * f[i]*fa[i] + v_factors[1] * fa[i] + add_factors));
			fitness_d0.push_back(w0 * exp(-null_delta*f[i]))                                                                                    ;
		}
	}

	/* INDIVIDUAL ERRORS CALCULATION */
	Vector ei0 (fitness - fitness_d0);
	Vector eid (fitness - fitness_d );
	for (size_t i(0); i<n; ++i) {
		coefficient ei20 = ei0[i] * ei0[i];
		coefficient ei2d = eid[i] * eid[i];
		Xi.push_back(ei20-ei2d); // measures how much the prediction of fitness for individual i imprved by including purging.
	}

	MD = mean(Xi);
	for (size_t i(0); i<Xi.size(); ++i) Yi.push_back(Xi[i]-MD);

	/* BOOTSTRAP PVALUE CALCULATION */
	if (model != ID) {
		std::uniform_int_distribution<> dist(0, Yi.size() - 1); // for random sampling of integers on the half-closed interval [0, Yi.size())
		for (size_t i(0); i<boot; ++i) {
			Vector YiB;
			for (size_t j(0); j<Yi.size(); ++j) {
				YiB.push_back(Yi[dist(random_generator)]);
			}
			coefficient MYB = mean(YiB);
			if (MYB >= MD) pvalue += 1.0 ;
		}
	}

	pvalue /= boot;
	return pvalue;
}
