/* =======================================================================================
 *                                                                                       *
 *       Filename:  methods.cpp                                                          *
 *                                                                                       *
 *    Description:  This file contains the core functions of PURGd, including routines   *
 * 					to estimate the efective purging coefficient, and other parameters   *
 * 					affecting to fitness under inbreeding and purging.                   *
 *                                                                                       *
 *                  -> nnlr_method ()                                                    *
 *                                                                                       *
 ======================================================================================= */

#include "methods.h"


/* =======================================================================================
 * Function nnlr_method ()
 * ---------------------------------------------------------------------------------------
 * Leads the program to estimate the best purging coefficient in a given pedigree file, by
 * using a numerical non-linear regression (NNLR) method. Here we use Artificial Bee Colony
 * (ABC) algorithm.     
 ======================================================================================= */
void nnlr_method (name filename, Settings& settings){

	/* ABC VARIABLES */
	size_t N (settings.get_nbees()); // number of bees per iteration
	int limit (settings.get_limit()); // limit number of iterations (of convergence)
	std::vector <Nectar> pantry , null_pantry; // it saves the best_honey (under same conditions, for different number of runs)

	/* OTHER LOCAL VARIABLES */
	Model model (settings.get_model());
	if (model !=IP) settings.set_d(0.0);
	bool fa_model (settings.get_fa_model());
	size_t nruns (settings.get_nruns());
	coefficient accuracy (settings.get_accuracy());
	bool maternal (settings.get_use_maternal());
	bool use_factors (settings.get_use_factors());
	size_t N_factors (0);
	pMatrix M_factors; // vector of pointers to matrix of additional factors (with length equal to number of d values)
	pMatrix null_M_factors; // idem, for the non-purging model
	bool repeat (false);

	/* READING PEDIGREE */
	Pedigree pedigree (filename);
	pedigree.update(settings);
	pVector G (pedigree.get_g());
	Vector fitness (pedigree.get_fitness());
	if (settings.is_delta_nopurged()) repeat = true;

	/* MATRIX OF ADDITIONAL FACTORS ([fa]+[gd]+[af]) */
	M_factors = pedigree.matrix_of_factors(settings, model);
	null_M_factors = pedigree.matrix_of_factors(settings, ID);
	if (M_factors.size()) N_factors = max(*M_factors[0]);

	/* ABC ALGORITHM : INITIALIZATION */
	Field hyperspace (pedigree.get_w0().value, N_factors,settings);
	Field null_space (hyperspace);
	null_space.set_de_dimension({0.0});
	if (fa_model) null_space.rm_fa_factors(model);
	if (model != IP) hyperspace.set_de_dimension({0.0});

	/* ABC ALGORITHM : SEARCH */
	for (size_t i(0); i < nruns; ++i) {

		Hive colmena      (N,hyperspace,fitness,G,M_factors, limit, accuracy);
		Hive null_colmena (N,null_space,fitness,G,null_M_factors, limit, accuracy);
		if (VERBOSE) {
			std::cout << std::fixed;
			std::cout << std::setprecision(2);
			pequal (pe, '=');
			std::cout << "ABC RUN " << i+1 << " / " << nruns;
			if (repeat) std::cout << " (pre-computing delta)";
			std::cout << std::endl;
			std::cout << "I" << "\t" << "RSS" << "\t" << "d" << "\t" << "w0" << "\t";
			if (model==IP) std::cout << "b(g)";
			else std::cout << "b(F)";
			if (model!=IP) {
				if      (model==BA) std::cout << "\t" << "b(ffa)";
				else if (model==BW) std::cout << "\t" << "b(fa)";
				else if (model==MX) std::cout << "\t" << "b(ffa)" << "\t" << "b(fa)";
			}
			if (maternal && model==IP) std::cout << "\t" << "b(gdam)";
			else if (maternal) std::cout << "\t" << "b(fdam)";
			if (use_factors) std::cout << "\t" << "(+ additional factors)";
			std::cout << std::endl;
		}

		std::size_t count (1);
		Nectar best_honey , best_null_honey; // best solution
		do {
			colmena.employers (hyperspace); colmena.onlookers (); colmena.scouts (hyperspace);
			null_colmena.employers (null_space); null_colmena.onlookers(); null_colmena.scouts(null_space);
			colmena.best_source(); null_colmena.best_source();
			if (VERBOSE) {
				best_honey = colmena.get_best_source();
				std::cout << count << "\t" << best_honey.stats.RSS << "\t" << best_honey.d << "\t" << best_honey.w0.value << "\t" << -best_honey.delta.value;
				if (fa_model | (maternal & !fa_model)) std::cout << "\t" << best_honey.reg_coefficients[0].value;
				if (model==MX) std::cout << "\t" << best_honey.reg_coefficients[1].value;
				if (maternal & fa_model) {
					if (model==MX) std::cout << "\t" << best_honey.reg_coefficients[2].value;
					else std::cout << "\t" << best_honey.reg_coefficients[1].value;
				}
				if (use_factors) std::cout << "\t" << "(...)";
				std::cout << std::endl;
				++count;
			}
		} while (!colmena.converges(limit));
		best_honey = colmena.get_best_source(); pantry.push_back (best_honey);
		best_null_honey = null_colmena.get_best_source(); null_pantry.push_back(best_null_honey);
	}

	kill_pMatrix (M_factors);
	kill_pMatrix (null_M_factors);

	if (!repeat) {
		save_nnlr (pedigree, pantry, null_pantry, settings);
		Nectar best_honey (pantry[0]);
		if (pantry.size()>1) best_honey = nectar_mean(pantry);
		if (settings.get_save_db()) save_database(pedigree, settings, best_honey.d);
	} else {
		/* RUN NNLR AGAIN WITH THE ESTIMATE OF DELTA */
		settings.set_model(IP);
		settings.set_d();
		Nectar best_honey = pantry[0];
		if (pantry.size()>1) best_honey = nectar_mean (pantry);
		settings.set_num_delta (best_honey.delta.value);
		nnlr_method (filename, settings);
	}

}
