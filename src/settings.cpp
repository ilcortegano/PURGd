/* =======================================================================================
 *                                                                                       *.
 *    Filename:   settings.cpp                                                           *
 *                                                                                       *
 *    Description: It contains functions to read and get values from settings file.      *
 *                                                                                       *
 *                 -> Settings::Settings ()                                              *
 * 	               -> Settings::get_* ()                                                 *
 *                 -> Settings::set_* ()                                                 *
 *                 -> Settings::read_arguments ()                                        *
 *                 -> Settings::read_settings ()                                         *
 *                 -> Settings::print_help ()                                            *
 *                 -> Settings::check ()                                                 *
 *                                                                                       *
 ======================================================================================= */

#include "settings.h"


/* ********************************************************************************************
 * Settings::Settings
 * --------------------------------------------------------------------------------------------
 * Class Settings constructor.
 * ********************************************************************************************/
Settings::Settings ()    {

	/* SET DEFAULT VALUES */
	seed = time(NULL);
	set_w0_noinbred = DEFAULT_SET_W0_NOINBRED;
	set_w0_num = DEFAULT_SET_W0_NUM;
	set_w0 = DEFAULT_SET_W0;
	set_delta_nopurged = DEFAULT_SET_DELTA_NOPURGED;
	set_delta_num = DEFAULT_SET_DELTA_NUM;
	set_delta = DEFAULT_SET_DELTA;
	set_d ();
	fixed_d = false;
	store_fixed_d = 0.0;
	use_maternal = false;
	use_factors = false;
	accuracy = DEFAULT_ACCURACY;
	max_w0 = DEFAULT_MAX_W0;
	max_delta = DEFAULT_MAX_DELTA;
	range_factors.min = DEFAULT_MIN_FACTORS;
	range_factors.max = DEFAULT_MAX_FACTORS;
	save_log = false;
	save_db = false;
	save_db_all = false;
	VERBOSE = false;
	method = DEFAULT_METHOD;
	model = DEFAULT_MODEL;
	genedrop = false;
	n_genedrop = DEFAULT_NGENEDROP;
	fa_model = false;
	nbees = DEFAULT_NBEES;
	limit = DEFAULT_LIMIT;
	nruns = DEFAULT_NRUNS;

}

/* ********************************************************************************************
 * Settings::read_settings ()
 * --------------------------------------------------------------------------------------------
 * If a settings file is specified, it read arguments from it. These arguments will change the 
 * default behaviour of PURGd. However, remember that command line parameters will have preference.
 * ********************************************************************************************/
void Settings::read_settings ( name filename ) {

	name value;
	sMatrix array;
	std::ifstream file (filename);

	if (file.is_open() ) {

		while ( getline (file, value) ) {

			for (size_t i(0); i<value.size(); ++i) { if (value[i] == '#') { value.clear(); break;} }

			if (value != "") {

				sVector ROW;
				std::istringstream iss (value);
				name result;
				while (getline(iss, result, '=')) {
					if (result.size() && result[result.size()-1] == '\r') result = result.substr( 0, result.size() - 1 );
					ROW.push_back(result.c_str())                                                                       ;
				}
				array.push_back(ROW); 
			}
		}
		file.close();		
	} else throw_error(ERROR_SETF);

	/* CHECK SETTINGS VALUES: Defaults may be modified following settings.txt */
	size_t N (array.size());

	for (size_t i(0); i<N; ++i) {

		size_t n_values (array[i].size()-1);
		name variable (array[i][0]);
		name argument;
		if (n_values) argument = array[i][1];

		// Read analysis method
		if (variable.empty() || (n_values && argument.empty())) {
			continue;

		// Read purging model
		} else if ( variable == "MODEL" ) {
			if (n_values) {
				if      (argument == "BW" || argument == "\"BW\"") { model = BW; fa_model = true; }
				else if (argument == "BA" || argument == "\"BA\"") { model = BA; fa_model = true; }
				else if (argument == "MX" || argument == "\"MX\"") { model = MX; fa_model = true; }
				else if (argument != "IP" && argument != "\"IP\"") throw_error (ERROR_PMOD);
			}

		// Read use of genedrop
		} else if ( variable == "GENEDROP" ) {
			if (n_values && string_to_bool (argument)) {
				genedrop = true;
				if (array[i].size()>1) {
					n_genedrop = atoi (argument.c_str() );
				}
			}

		// Read fixed value of purging coefficient
		} else if ( variable == "D" ) {
			if (n_values) {
				if (isNumeric(argument.c_str())) {
					d.min = atof (argument.c_str());
					d.max = d.min;
					d.step = 0.0;
					fixed_d = true;
					store_fixed_d = d.min;
				} else throw_error (ERROR_DRAN);
			}

		// Read initial fitness estimation method
		} else if ( variable == "W0" ) {
			if (n_values) {
				if (argument=="\"noinbred\"" || argument=="noinbred") set_w0_noinbred = true;
				else if (isNumeric(argument)) {
					set_w0_num = true;
					set_w0 = atof(argument.c_str());
					set_w0_noinbred = false;
					if (set_w0 <= 0.0) throw_error(ERROR_USEW);
				} else if (argument=="") {
					set_w0_noinbred = false;
					set_w0_num = false;
				} else throw_error(ERROR_USEW);
			}

		// Read estimation method for inbreeding depression rate		
		} else if ( variable == "BG" ) {
			if (n_values) {
				if (argument=="\"nopurged\"" || argument=="nopurged") set_delta_nopurged = true;
				else if (isNumeric(argument)) {
					set_delta_num = true;
					set_delta = atof(argument.c_str());
					if (set_delta <= 0.0) throw_error(ERROR_USEB);
				} else throw_error(ERROR_USEB);
			}

		// Read use of maternal inbreeding
		} else if ( variable == "MATERNAL" ) {
			if (n_values && string_to_bool (argument)) {
				use_maternal = true;
				if (!is_in(argument,{"0","1"})) throw_error(ERROR_USEM);
			}

		// Read the column number for additional numeric factors
		} else if ( variable == "ADD_FACTORS" ) {
			if (n_values) {
				use_factors = true;
				sVector tmp_factors (split_vector (argument, ',' ));
				for (size_t j(0); j < tmp_factors.size(); ++j) {
					factors.push_back( atoi ( tmp_factors[j].c_str() ) - 1 ); // column index in factors vector equals -1 the index in settings.txt
				}
			}

		// Read the column number for additional categorical factors
		} else if ( variable == "ADD_CFACTORS" ) {
			if (n_values) {
				use_factors = true;
				sVector tmp_factors (split_vector (argument, ',' ));
				for (size_t j(0); j < tmp_factors.size(); ++j) {
					nominal.push_back( atoi ( tmp_factors[j].c_str() ) - 1 ); // column index in factors vector equals -1 the index in settings.txt
				}
			}

		// Read the name of additional numeric factors
		} else if ( variable == "NAME_FACTORS" ) {
			if (n_values) fnames = split_vector (argument, ',' );

		// Read the name of additional categorical factors
		} else if ( variable == "NAME_CFACTORS" ) {
			if (n_values) nnames = split_vector (argument, ',' );

		// Read use of a log file
		} else if ( variable == "SAVE_LOG" ) {
			if (n_values && string_to_bool (argument)) {
				save_log = true;
				if (n_values && argument != "1") {
					log = argument;
					if (log.front() != '\"' && log.front() != log.back() ) {
						throw_error(ERROR_LOGF);
					} else {
						log.pop_back();
						log.erase(0,1);
					}
				}
			}

		// Read use of databases
		} else if ( variable == "SAVE_DB" ) {
			if (n_values) {
				if (argument=="ALL") {
					save_db = true;
					save_db_all = true;
				} else if (string_to_bool (argument)) {
					save_db = true;
				}
				if (!is_in(argument,{"0","1","ALL"})) throw_error(ERROR_SVDB);
			}

		// Read accuracy value
		} else if ( variable == "ACCURACY" ) {
			if (n_values) {
				accuracy = atof (argument.c_str() );
			}

		// Read SEED
		} else if ( variable == "SEED" ) {
			if (n_values ) {
				seed = atoi (argument.c_str());
			}

		// Read use of verbose mode (print output to terminal)
		} else if ( variable == "VERBOSE" ) {
			if (n_values && string_to_bool (argument)) {
				VERBOSE = true;
			}


		// Read number of bees in ABC algorithm
		} else if ( variable == "NBEES" ) {
			if (n_values) {
				nbees = atoi (argument.c_str() );
			}

		// Number of iterations (of convergence) in ABC
		} else if ( variable == "LIMIT" ) {
			if (n_values) {
				limit = atoi (argument.c_str() );
			}

		// Read number of runs in NNLR method
		} else if ( variable == "NRUNS" ) {
			if (n_values) {
				nruns = atoi (argument.c_str() );
			}

		// Read maximum value for the slope on w0 in NNLR method
		} else if ( variable == "MAX_W0" ) {
			if (n_values) {
				max_w0 = atof (argument.c_str() );
			}

		// Read maximum value for the slope on g(d) in NNLR method
		} else if ( variable == "MAX_BG" ) {
			if (n_values) {
				max_delta = atof (argument.c_str() );
			}

		// Read maximum and minimum values for other factors in NNLR method
		} else if ( variable == "RANGE_FACTORS" ) {
			if (n_values) {
				sVector ranges = split_vector (argument, ',');
				range_factors.min = atof ( ranges[0].c_str() )   ;
				range_factors.max   = atof ( ranges[1].c_str() ) ;
			}

		// If a different variable is present, the software breaks
		} else {
			throw_error(ERROR_SREA);
		}
	}

}

/* ********************************************************************************************
 * Settings::read_arguments ()
 * --------------------------------------------------------------------------------------------
 * Read arguments passed to PURGd, modifying settings attributes. If different values are given
 * in setting.txt and from the terminal (arguments), this former are used, but they do not 
 * overwrite settings file.
 * ********************************************************************************************/
void Settings::read_arguments ( int N, char *arg[] ) {

	/* LOCAL VARIABLES THAT CONTROL WHETHER A PARAMETER HAS BEEN CHANGED OR NOT */
	bool c_file (false);
	bool c_seed (false);
	bool c_method (false);
	bool c_model (false);
	bool c_genedrop (false);
	bool c_nbees (false);
	bool c_limit (false);
	bool c_nruns (false) ;
	bool c_set_w0 (false);
	bool c_set_d (false);
	bool c_set_delta_num (false);
	bool c_set_delta_nopurged (false);
	bool c_use_maternal (false);
	bool c_factors (false);
	bool c_nominal (false);
	bool c_fnames (false);
	bool c_nnames (false);
	bool c_frange (false);
	bool c_accuracy (false);
	bool c_max_w0 (false);
	bool c_max_delta (false);
	bool c_savelog (false);
	bool c_savedb (false);
	bool c_verbose (false);

	/* CHECK LAST ARGUMENT FILE */
	const std::string first_arg = std::string(arg[N-1]); // at least one argument is required, either "--help" or an input file.
	std::ifstream filename (first_arg);
	if (N==1) print_help (false);
	else if (first_arg == "--help")	print_help (true);

	/* OPEN SETTINGS FILE (if specified) */
	for (int i(0); i<N; ++i) {
		sVector v_argument (split_vector ((arg[i]), '='));
		if (v_argument[0] == "--config") {
			if (v_argument.size()!=2) {
				throw_error (ERROR_CONF);
			}
			this->read_settings (v_argument[1]); // call an error if the file does not exist, or if the format is wrong.
			break;
		}
	}

	/* SET PURGING PARAMETER RANGES */
	w0.min = 0.0;
	w0.max = max_w0;
	w0.step = accuracy;

	delta.min = 0.0;
	delta.max = max_delta;
	delta.step  = accuracy;

	/* SET FACTOR VARIABLES*/
	Nfactors = factors.size() + nominal.size();
	range_factors.step = accuracy;

	/* READ ARGUMENTS, EXCLUDING INPUT FILE */
	for (int i(1); i < N-1; ++i) {

		sVector v_argument = split_vector (arg[i], '=');
		name argument (v_argument[0]);
		name value;
		if (v_argument.size() > 1) value = v_argument[1];

		// double-dash options
		if (argument[0] == '-' && argument[1] == '-') {

			if (argument == "--config") {
				if (c_file)	throw_error (ERROR_DUPL);
				c_file = true;
			}

			else if (argument == "--nnlr" || argument == "--lr") {
				if (c_method) throw_error (ERROR_DUPL);
				c_method = true;
				if (argument == "--nnlr") method = NNLR;
				else if (argument == "--lr") throw_error(ERROR_NOLR);
			}

			else if (argument == "--ip" || argument == "--ffa" || argument == "--fa" || argument == "--faffa" ) {
				if (c_model) throw_error (ERROR_DUPL);
				c_model = true;
				if (argument == "--ip") model = IP;
				else if (argument == "--ffa") { model = BA; fa_model = true; }
				else if (argument == "--fa") { model = BW; fa_model = true; }
				else if (argument == "--faffa") { model = MX; fa_model = true; }
			}

			else if (argument == "--genedrop") {
				if (c_genedrop) throw_error (ERROR_DUPL);
				c_genedrop = true;
				genedrop = true;
				if (v_argument.size() > 1) {
					n_genedrop = atoi (value.c_str());
				}
			}

			else if (argument == "--w0") {
				if (c_set_w0) throw_error (ERROR_DUPL);
				c_set_w0 = true;
				if (isNumeric(value)) {
					set_w0_num = true;
					set_w0 = atof(value.c_str());
					set_w0_noinbred = false;
				} else if (value=="") {
					set_w0_num = false;
					set_w0_noinbred = false;	
				} else throw_error(ERROR_USEW);
			}

			else if (argument == "--d") {
				if (c_set_d) throw_error (ERROR_DUPL);
				c_set_d = true;
				if (isNumeric(value)) {
					d.min = atof (value.c_str());
					d.max = d.min;
					d.step = 0.0;
					fixed_d = true;
					store_fixed_d = d.min;
				} else throw_error (ERROR_DRAN);
			}

			else if (argument == "--delta") {
				if (c_set_delta_num) throw_error (ERROR_DUPL);
				c_set_delta_num = true;
				if (isNumeric(value)) {
					set_delta_num = true;
					set_delta = atof(value.c_str());
					if (set_delta <= 0.0) throw_error(ERROR_USEB);
				} else throw_error(ERROR_USEB);
			}

			else if (argument == "--delta-nopurged") {
				if (c_set_delta_nopurged) throw_error (ERROR_DUPL);
				c_set_delta_nopurged = true;
				set_delta_nopurged = true;
			}

			else if (argument == "--maternal") {
				if (c_use_maternal) throw_error (ERROR_DUPL);
				c_use_maternal = true;
				use_maternal = true;
			}

			else if (argument == "--factor.cols") {
				if (c_factors) throw_error (ERROR_DUPL);
				c_factors = true;
				use_factors = true;
				factors.clear();
				sVector tmp_argument (split_vector (v_argument, ','));
				for (size_t j(1); j<tmp_argument.size(); ++j) {
					factors.push_back(atoi(tmp_argument[j].c_str()) -1 ); // column index in factors vector equals -1 the index passed as argument
				}
			}

			else if (argument == "--factor.names") {
				if (c_fnames) throw_error (ERROR_DUPL);
				c_fnames = true;
				use_factors = true;
				fnames.clear();
				sVector tmp_argument (split_vector (v_argument, ','));
				for (size_t j(1); j<tmp_argument.size(); ++j) {
					fnames.push_back(tmp_argument[j]);
				}
			}

			else if (argument == "--cfactor.cols") {
				if (c_nominal) throw_error (ERROR_DUPL);
				c_nominal = true;
				use_factors = true;
				nominal.clear();
				sVector tmp_argument (split_vector (v_argument, ','));
				for (size_t j(1); j<tmp_argument.size(); ++j) {
					nominal.push_back(atoi(tmp_argument[j].c_str()) -1 ); // column index in factors vector equals -1 the index passed as argument
				}
			}

			else if (argument == "--cfactor.names") {
				if (c_nnames) throw_error (ERROR_DUPL);
				c_nnames = true;
				use_factors = true;
				nnames.clear();
				sVector tmp_argument (split_vector (v_argument, ','));
				for (size_t j(1); j<tmp_argument.size(); ++j) {
					nnames.push_back(tmp_argument[j]);
				}
			}

			else if (argument == "--save-log") {
				if (c_savelog) throw_error (ERROR_DUPL);
				c_savelog = true;
				save_log = true;
				if (v_argument.size() > 1) {
					log = value;
				}
			}

			else if (argument == "--save-db") {
				if (c_savedb) throw_error (ERROR_DUPL);
				c_savedb = true;
				save_db = true;
			}

			else if (argument == "--save-db-all") {
				if (c_savedb) throw_error (ERROR_DUPL);
				c_savedb = true;
				save_db = true;
				save_db_all = true;
			}

			else if (argument == "--accuracy") {
				if (c_accuracy) throw_error (ERROR_DUPL);
				c_accuracy = true;
				accuracy = atof (value.c_str());
			}

			else if (argument == "--seed") {
				if (c_seed) throw_error (ERROR_DUPL);
				c_seed = true;
				seed = atoi (value.c_str());
			}

			else if (argument == "--verbose") {
				if (c_verbose) throw_error (ERROR_DUPL);
				c_verbose = true;
				VERBOSE = true;
			}

			else if (argument == "--nbees") {
				if (c_nbees) throw_error (ERROR_DUPL);
				c_nbees = true;
				nbees = atoi (value.c_str());
			}

			else if (argument == "--limit") {
				if (c_limit) throw_error (ERROR_DUPL);
				c_limit = true;
				limit = atoi (value.c_str());
			}

			else if (argument == "--nruns") {
				if (c_nruns) throw_error (ERROR_DUPL);
				c_nruns = true;
				nruns = atoi (value.c_str());
			}

			else if (argument == "--max-w0") {
				if (c_max_w0) throw_error (ERROR_DUPL);
				c_max_w0 = true;
				max_w0 = atof (value.c_str());
			}

			else if (argument == "--max-delta") {
				if (c_max_delta) throw_error (ERROR_DUPL);
				c_max_delta = true;
				max_delta = atof (value.c_str());
			}

			else if (argument == "--factor.range") {
				if (c_frange) throw_error (ERROR_DUPL);
				c_frange = true;
				sVector ranges (split_vector (v_argument, ','));
				range_factors.min = atof (ranges[1].c_str());
				range_factors.max = atof (ranges[2].c_str());
			}

			else throw_error(ERROR_PSIN); // the argument is wrong

		// single-dash options
		} else if (argument[0] == '-' && argument[0] != '-') {
			// do something
			exit(-1);
		} else {
			throw_error(ERROR_PSIN);
		}
	}

	/* UPDATE PARAMETERS */
	if (!c_set_d) d.step = accuracy;
	w0.step = accuracy;
	delta.step = accuracy;
	range_factors.step = accuracy;
	w0.max = max_w0;
	delta.max = max_delta;
	Nfactors = factors.size() + nominal.size();

}

/* ********************************************************************************************
 * Settings::check()
 * --------------------------------------------------------------------------------------------
 * This function halts the program in the case that the combination of some settings parameters
 * lead to an execution out of the scope of this software.
 * ********************************************************************************************/
void Settings::check () {

	// Check value and range of some parameters
	if (accuracy <= 0 || accuracy >= 0.5) throw_error (ERROR_ACCU);
	else if (max_w0 <= 0) throw_error (ERROR_MAXW);
	else if (max_delta <= 0) throw_error (ERROR_MAXB);
	else if (seed <= 0) throw_error (ERROR_SEED);
	else if (nbees < 250) throw_error(ERROR_NBEE);
	else if (limit < 10) throw_error(ERROR_LIMT);
	else if (nruns <=0) throw_error(ERROR_NRUN);
	else if (d.min < 0.0 || d.max > 0.5) throw_error (ERROR_DRAN);
	else if (range_factors.min >= range_factors.max)	throw_error (ERROR_RANF);
	else if (n_genedrop <= 0)	throw_error(ERROR_NGEN);
	else if (n_genedrop > 1000000000) throw_error(ERROR_MGEN);
	else if (is_in('/',log)) throw_error(ERROR_LOGN);

	// Some combinations of setting parameters are forbidden
	// NNLR-only options
	if (method!=NNLR) throw_error (ERROR_NOLR);
	if (method != NNLR) {
		if (nbees != DEFAULT_NBEES) throw_error (ERROR_NNLR_BEE);
		else if (limit != DEFAULT_LIMIT) throw_error (ERROR_NNLR_LIM);
		else if (max_w0 != DEFAULT_MAX_W0) throw_error (ERROR_NNLR_MW0);
		else if (max_delta != DEFAULT_MAX_DELTA) throw_error (ERROR_NNLR_MDT);
		else if (range_factors.min != DEFAULT_MIN_FACTORS || range_factors.max != DEFAULT_MAX_FACTORS) throw_error (ERROR_NNLR_AFT);
		else if (nruns != DEFAULT_NRUNS) throw_error (ERROR_NNLR_RUN);
	}

	// IP-only options
	if (model!=IP) {
		if (set_delta_num) throw_error(ERROR_NOIP_FAD);
		else if (set_delta_nopurged) throw_error(ERROR_NOIP_FAS);
		else if (fixed_d) throw_error(ERROR_FIXD);
	}

	// Check additional factors settings
	if (Nfactors != fnames.size() + nnames.size()) throw_error (ERROR_USEF_ODD);
	else if (use_factors && !Nfactors) throw_error (ERROR_USEF_ARG);
	else if (factors.size() != fnames.size()) throw_error (ERROR_USEF_ODD);
	else if (nominal.size() != nnames.size()) throw_error (ERROR_USEF_ODD);
	for (const auto& i : factors) if (i < 4) throw_error (ERROR_USEF_MIN);
	for (const auto& i : nominal) if (i < 4) throw_error (ERROR_USEF_MIN);
	if (repetition(factors) | repetition(fnames) | repetition(nominal) | repetition(nnames)) throw_error (ERROR_USEF_REP);
	else if (repetition(merge(factors,nominal)) | repetition(merge(fnames,nnames))) throw_error (ERROR_USEF_REP);
	else if ( !Nfactors && (range_factors.min != DEFAULT_MIN_FACTORS || range_factors.max != DEFAULT_MAX_FACTORS)) throw_error (ERROR_USEF_RNG);

}

/* =======================================================================================
 * Function print_help ()
 * ---------------------------------------------------------------------------------------
 * Prints information about the software. It may be a more detailed help or not.
 * Short information is shown when the software is executed without arguments.
 * Details are printed if '--help' option is used.
 * =====================================================================================*/
void print_help ( bool detailed ) {

	pequal(pe,'=');
	std::cout << "PURGd v2.2 (03/03/2020)" << std::endl;
	std::cout << "Aurora García-Dorado, Jinliang Wang and Eugenio López-Cortegano (2016)." << std::endl;
	std::cout << "\t\"Predictive model and software for inbreeding-purging analysis of pedigreed populations\"." << std::endl << " \tG3: Genes, Genomes, Genetics 6 (11): 3593-3601." << std::endl;
	std::cout << std::endl;
	std::cout << "\tPURGd --help" << std::endl;
	std::cout << "\tPURGd [options] [--config=<settings file>] <input file>" << std::endl << std::endl;

	if (detailed) {

		std::cout << "The <input file> must be entered including its absolute or relative path, including the \".csv\" \n"
			  << "extension. Other options can be controlled from the settings file or the command line terminal."
			  << std::endl << std::endl << std::endl;

		std::cout << "= PURGd v2.2 OPTIONS "; pequal(pe-23,'='); std::cout << std::endl;

		std::cout << "Inbreeding-purging (IP) model (García-Dorado 2012) is assumed by default. The following alternative" << std::endl
			  << "based on ancestral inbreeding are also supported:" << std::endl
			  << "\t--ffa   : Ballou's model (Ballou 1997)." << std::endl
			  << "\t--fa    : Boakes & Wang model (Boakes & Wang 2005)." << std::endl
			  << "\t--faffa : Ballou and Boakes & Wang mixed model." << std::endl
			  << std::endl;

		std::cout << "A list of other common options include:" << std::endl << std::endl

			  << "\t" << "* GENERAL OPTIONS" << std::endl
			  << "\t  --config=<F>       : Settings file to be used (including path)." << std::endl
			  << "\t  --seed=<INT>       : Set the seed to an integer value." << std::endl
			  << "\t  --verbose          : Verbose mode." << std::endl
			  << "\t  --accuracy=<NUM>   : Change default accuracy (0.01) to NUM. It must be lower than 0.5." << std::endl
			  << std::endl

			  << "\t" << "* ANALYSIS OPTIONS" << std::endl
			  << "\t  --genedrop=<INT>   : Use a genedrop simulation process (10e6 iterations by default)." << std::endl
			  << "\t  --d=<NUM>          : Set a fixed value of the purging coefficient." << std::endl
			  << "\t  --w0=<NUM>         : Give a numeric positive value for the initial average fitness." << std::endl
			  << "\t  --w0               : Estimate the initial fitness as any other regression coefficient." << std::endl
			  << "\t  --delta=<NUM>      : Give a numeric positive value for the inbreeding depression rate." << std::endl
			  << "\t  --delta-nopurged   : Estimate the inbreeding depression rate in individuals with non-inbred ancestors (Fa=0)." << std::endl
			  << "\t  --maternal         : Use maternal effects." << std::endl
			  << "\t  --factor.cols=<F>  : Use additional numeric factors: columns to be selected (comma-separated)." << std::endl
			  << "\t  --factor.names=<F> : Use additional numeric factors: factor names (comma-separated)." << std::endl
			  << "\t  --cfactor.cols=<F> : Use additional categorical factors: columns to be selected (comma-separated)." << std::endl
			  << "\t  --cfactor.names=<F>: Use additional categorical: factor names (comma-separated)." << std::endl
			  << std::endl

			  << "\t" << "* OUTPUT OPTIONS" << std::endl
			  << "\t  --save-log=<F>     : Save a log file." << std::endl
			  << "\t  --save-db          : Save a database output file for analysed individuals." << std::endl
			  << "\t  --save-db-all      : Save a database output file including all individuals." << std::endl
			  << std::endl

			  << "\t" << "* NNLR OPTIONS" << std::endl
			  << "\t  --nbees=<INT>      : Number of solutions to explore by iteration (default is 250)." << std::endl
			  << "\t  --limit=<INT>      : Required number of iterations without improving the solution (default is 10)." << std::endl
			  << "\t  --nruns=<INT>      : Change the number of times the ABC algorith is run (default is 1)." << std::endl
			  << "\t  --max-w0=<NUM>     : Sets the maximum value of initial fitness to explore (default is 1)." << std::endl
			  << "\t  --max-delta=<NUM>  : Sets the maximum value of the inbreeding depression rate (default is 10)." << std::endl
			  << "\t  --factor.range=<NUM,NUM> : Sets the minimum and maximum value to explore for the slope" << std::endl
			  << "\t                             for additional factors (default is -10,10)." << std::endl
			  << std::endl;

		std::cout << "Check the User's guide for a more detailed list of options, including options for the settings" << std::endl
				  << "file." << std::endl << std::endl;

		std::cout << "Users are encouraged to request additional features on the software and to report bugs." << std::endl
				  << "In that case, please contact Eugenio López-Cortegano (e.lopez-cortegano@ed.ac.uk) or Aurora " << std::endl
				  << "García-Dorado (augardo@ucm.es)." << std::endl << std::endl;

		std::cout << "= REFERENCES "; pequal(pe-13,'='); std::cout << std::endl;
				std::cout << "Ballou JD (1997). Ancestral inbreeding only minimally affects inbreeding depression in mammalian" << std::endl
						  << "\t" << "populations. Journal of Heredity 88 (3): 169-178." << std::endl << std::endl;
		std::cout << "Boakes E & Wang J (2005). A simulation study on detecting purging of inbreeding depression in" << std::endl
						  << "\t" << "captive populations. Genetics Research 86: 139-148." << std::endl << std::endl;
		std::cout << "García-Dorado A. et al (2016). Predictive model and software for inbreeding-purging analysis of" << std::endl
						  << "\t" << "pedigreed populations. G3: Genes, Genomes, Genetics 6 (11): 3593-3601." << std::endl << std::endl;
		std::cout << "García-Dorado A. (2012). Understanding and predicting the fitness decline of shrunk populations:" << std::endl
						 << "\t" << "inbreeding, purging, mutation, and standard selection. Genetics 190: 1461-1476." << std::endl << std::endl;

	} else std::cout << "Enter 'PURGd --help' to get more detailed information." << std::endl;
	pequal(pe,'=');
	exit(0);
}
