/* =======================================================================================
 *                                                                                       *
 *       Filename:  utils.cpp                                                            *
 *                                                                                       *
 *    Description:  This file contains complementary additional functions.               *
 *                                                                                       *
 *                  -> string_to_bool ()                                                 *
 *                  -> kill_pVector ()                                                   *
 *                  -> kill_pMatrix ()                                                   *
 *                  -> plimit ()                                                         *
 * 					-> isNumeric ()                                                      *
 *                  -> tr_model ()                                                       *
 *                  -> split_vector ()                                                   *
 *                                                                                       *
 ======================================================================================= */

#include "utils.h"


/* =======================================================================================
 * Function string_to_bool ()
 * ---------------------------------------------------------------------------------------
 * If string is "0" or "false" returns false, else returns true.
 ======================================================================================= */
bool string_to_bool (const name& string) {
	return !(string == "0" || string == "false");
}

/* =======================================================================================
 * Function kill_pVector & kill_pMatrix ()
 * ---------------------------------------------------------------------------------------
 * Emptys memory in elements of type pVector or pMatrix. 
 ======================================================================================= */
void kill_pVector (pVector& vector) {
	size_t length (vector.size());
	if (length) {
		for (size_t i(0); i<length; ++i) {
			delete vector[i]; vector[i] = nullptr;
		}
	}
	vector.clear();
}

/* ----------------------------------------------------------------------------------- */
void kill_pMatrix (pMatrix& vector) {
	if (vector.size()) {
		for (size_t i(0), length(vector.size()); i<length; ++i) {
			delete vector[i]; vector[i] = nullptr;
		}
	} 
	vector.clear();
}

/* =======================================================================================
 * Function plimit ()
 * ---------------------------------------------------------------------------------------
 * When printing statistic parameters as F and t's p values, it returns the limit of
 * precission instead of zero.
 ======================================================================================= */
name plimit (coefficient parameter) {

	if ( abs(parameter) < 0.0000000000000001) return "1.0e-16";
	else {
		#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
		( std::ostringstream() << std::dec << x ) ).str()
		name s_parameter (SSTR(parameter));
		#undef SSTR
		return s_parameter;
	}
}

/* =======================================================================================
 * Function isNumeric ()
 * ---------------------------------------------------------------------------------------
 * Returns true if all elements in a string object are numeric.
 ======================================================================================= */
bool isNumeric(const std::string& input) {
	if (input=="") return false;
	return (strspn (input.c_str(), "-.0123456789")==input.size());
}

/* =======================================================================================
 * Function any ()
 * ---------------------------------------------------------------------------------------
 * Returns true if at least one element is true.
 ======================================================================================= */
bool any (const bVector& v) {
	for (const bool& i:v) if (i) return true;
	return false;
}

/* =======================================================================================
 * Function tr_model ()
 * ---------------------------------------------------------------------------------------
 * Returns the complete name of a genetic model
 ======================================================================================= */
name tr_model (Model model) {
	 if      (model == IP) return "Inbreeding-purging";
	 else if (model == BA) return "Ballou's"          ;
	 else if (model == BW) return "Boakes & Wang's"   ;
	 else if (model == MX) return "Mixed"             ;
	 else return "No purging";
 }

/* =======================================================================================
 * Function split_vector ()
 * ---------------------------------------------------------------------------------------
 * It takes a string and splits it using a given character. Each fragment is saved into a 
 * vector
 * =====================================================================================*/
sVector split_vector (name string, char character) {

	sVector result                 ;
	std::istringstream iss (string);
	name piece                     ;

	while (getline(iss, piece, character)) result.push_back(piece.c_str());

	return result;
}

/* ----------------------------------------------------------------------------------- */
sVector split_vector (sVector vector, char character) {

	sVector result;

	for (const auto& i : vector) {
		std::istringstream iss (i);
		name piece                ;
		while (getline(iss, piece, character)) {
			result.push_back(piece.c_str());
		}
	}

	return result;
}
