/* =======================================================================================
 *                                                                                       *
 *    Filename:    genedrop.cpp                                                          *
 *                                                                                       *
 *    Description: Functions are defined to estimate inbreeding using a simulation       *
 *                 process (genedrop).                                                   *
 *                                                                                       *
 *                 -> sim_allele ()                                                      *
 *                 -> ibd ()                                                             *
 *                 -> read_F ()                                                          *
 *                 -> read_Fa ()                                                         *
 *                 -> genedrop_neutral ()                                                *
 *                                                                                       *
 ======================================================================================= */
 
#include "genedrop.h"


/* =======================================================================================
 * Function sim_allele ()
 * ---------------------------------------------------------------------------------------
 * Simulates heritage from one parental 'par' (1:dam, 2:sir) to the individual 'ind'
 * =====================================================================================*/
size_t sim_allele (const iMatrix& pedigree, size_t ind, size_t par, size_t& count, Unif& dist) {

	/* LOCAL VARIABLES */
	size_t allele (0); // simulated allele
	size_t p_ind (pedigree[ind][par]); // parental of ind

	// inheritance if the parental is unknown
	if (!p_ind) {
		allele = count;
		++ count;
	// inheritance if the parental is homozygous
	} else if (pedigree[p_ind-1][da_col] == pedigree[p_ind-1][sa_col]) {
		allele = pedigree[p_ind-1][da_col];
	// inheritance if the parental is heterozygous 
	} else {
		coefficient random = dist(random_generator);
		if (random < 0.5) {
			allele = pedigree[p_ind-1][da_col];
		} else {
			allele = pedigree[p_ind-1][sa_col];
		}
	}

	return allele;
}

/* =======================================================================================
 * Function ibd ()
 * ---------------------------------------------------------------------------------------
 * Modifies the genotyped pedigree matrix adding two columns that keep track of IBD alleles
 * =====================================================================================*/
void ibd (iMatrix& g_pedigree) {

	for (size_t i(0); i< g_pedigree.size(); ++i) {
		
		size_t i_d (g_pedigree[i][1]); // i's mother
		size_t i_s (g_pedigree[i][2]); // i's father

		// maternal allele
		// if i's mother is unknown, allele is not IBD
		if (g_pedigree[i][1]==0) {
			g_pedigree[i].push_back(0);
		// if i's mother is homozygous, allele is IBD
		} else if (g_pedigree[i_d-1][da_col] == g_pedigree[i_d-1][sa_col]) {
			g_pedigree[i].push_back(1);
		// if i's maternal allele is 's mother maternal allele, and it was IBD
		} else if ((g_pedigree[i][da_col] == g_pedigree[i_d-1][da_col]) && g_pedigree[i_d-1][da_ibd]) {
			g_pedigree[i].push_back(1);
		// if i's maternal allele is 's mother paternal allele, and it was IBD
		} else if ((g_pedigree[i][da_col] == g_pedigree[i_d-1][sa_col]) && g_pedigree[i_d-1][sa_ibd]) {
			g_pedigree[i].push_back(1);
		} else {
			g_pedigree[i].push_back(0);
		}
		// idem for paternal allele
		if (g_pedigree[i][2]==0) {
			g_pedigree[i].push_back(0);
		} else if (g_pedigree[i_s-1][da_col] == g_pedigree[g_pedigree[i][2]-1][sa_col]) {
			g_pedigree[i].push_back(1);
		} else if ((g_pedigree[i][sa_col] == g_pedigree[i_s-1][da_col]) && g_pedigree[i_s-1][da_ibd]) {
			g_pedigree[i].push_back(1);
		} else if ((g_pedigree[i][sa_col] == g_pedigree[i_s-1][sa_col]) && g_pedigree[i_s-1][sa_ibd]) {
			g_pedigree[i].push_back(1);
		} else {
			g_pedigree[i].push_back(0);
		}
	}
}

/* =======================================================================================
 * Function read_F ()
 * ---------------------------------------------------------------------------------------
 * This function reads the genotyped pedigree matrix and gives a value of F for each 
 * individual. An individual has F=1 if is homozygous, F=0 otherwise.
 * =====================================================================================*/
Vector read_F (const iMatrix& g_pedigree) {

	Vector F;
	for (size_t i(0); i<g_pedigree.size(); ++i) {
		if (g_pedigree[i][da_col] == g_pedigree[i][sa_col]) {
			F.push_back(1);
		} else {
			F.push_back(0);
		}
	}
	return F;
}

/* =======================================================================================
 * Function read_Fa ()
 * ---------------------------------------------------------------------------------------
 * This function reads the genotyped pedigree matrix and gives a value of Fa for each 
 * individual. An individual sums 0.5 to Fa for each allele that has been in homocygosis
 * in an ancestor.
 * =====================================================================================*/
Vector read_Fa (const iMatrix& g_pedigree) {

	Vector Fa (g_pedigree.size(),0.0);

	/* INDIVIDUALS CARRYING ALLELES THAT WHERE IBD IN AN ANCESTOR HAVE Fa */
	for (size_t i(0); i<g_pedigree.size(); ++i) {

		size_t i_d (g_pedigree[i][1]); // i's mother
		size_t i_s (g_pedigree[i][2]); // i's father

		// if i's mother is homozygous, or has Fa=1, the allele inherited from her is IBD
		if (i_d && ((g_pedigree[i_d-1][da_col] == g_pedigree[i_d-1][sa_col]) || Fa[i_d-1] == 1.0 )) {
			Fa[i] += 0.5;
		// if i's mother is heterozygous, and only one of her alleles was IBD
		} else if (i_d && Fa[i_d-1] == 0.5) {
			// if the allele is the maternal allele of the mother, and is IBD
			if (g_pedigree[i_d-1][da_ibd] && g_pedigree[i_d-1][da_col] == g_pedigree[i][da_col]) {
				Fa[i] += 0.5;
			// if the allele is the paternal allele of the mother, and is IBD
			} else if (g_pedigree[i_d-1][sa_ibd] && g_pedigree[i_d-1][sa_col] == g_pedigree[i][da_col]) {
				Fa[i] += 0.5;
			}
		}

		// idem for the father
		if (i_s && ((g_pedigree[i_s-1][sa_col] == g_pedigree[i_s-1][da_col]) || Fa[i_s-1] == 1.0 )) {
			Fa[i] += 0.5;
		} else if (i_s && Fa[i_s-1] == 0.5) {
			if (g_pedigree[i_s-1][sa_ibd] && g_pedigree[i_s-1][sa_col] == g_pedigree[i][sa_col]) {
				Fa[i] += 0.5;
			} else if (g_pedigree[i_s-1][da_ibd] && g_pedigree[i_s-1][da_col] == g_pedigree[i][sa_col]) {
				Fa[i] += 0.5;
			}
		}
	}

	return Fa;
}

/* =======================================================================================
 * Function genedrop ()
 * ---------------------------------------------------------------------------------------
 * Estimates the inbreeding coefficient(F) by simulating heritage of alleles with a given
 * frequency (q) at the base non-inbred population.
 * =====================================================================================*/
Vector genedrop (const iMatrix& pedigree, size_t n_drops) {

	Unif dist(0.0, 1.0); // uniform distribution
	Vector F; // neutral inbreeding coefficient
	Vector Fa; // ancestral inbreeding coefficient

	if (VERBOSE) pequal(pe,'=');

	// GENEDROPPING
	for (size_t iteration (0); iteration < n_drops; ++iteration) {

		if (VERBOSE) std::cout << "\rRunning gene drop simulation... " << iteration+1 << " / " << n_drops;

		// each iteration a genotyped matrix is created containing alleles inherited from the dam and sire
		iMatrix genotyped_pedigree (pedigree);
		size_t n_alleles (0)                 ;
		for (size_t i(0); i<pedigree.size(); ++i) {
			genotyped_pedigree[i].push_back(sim_allele(genotyped_pedigree,i,1,n_alleles,dist));
			genotyped_pedigree[i].push_back(sim_allele(genotyped_pedigree,i,2,n_alleles,dist));
		}
		ibd(genotyped_pedigree);
		if (iteration) {
			Fa += read_Fa (genotyped_pedigree);
		} else {
			Fa = read_Fa (genotyped_pedigree);
		}
	}
	if (VERBOSE) std::cout << std::endl;

	// Avarage ancestral inbreeding (Fa)
	for (auto& i:Fa) {
		i /= n_drops;
	}

	return Fa;
}

