#ifndef SETTINGS_H
#define SETTINGS_H

// =====   HEADER FILE INCLUDES   =====

#include <fstream>
#include <time.h>
#include "utils.h"
#include "templates.h"


// =======   GLOBAL  VARIABLES  =======

extern bool VERBOSE;

constexpr bool DEFAULT_SET_W0_NOINBRED = true;
constexpr bool DEFAULT_SET_W0_NUM = false;
constexpr coefficient DEFAULT_SET_W0 = 1.0;
constexpr bool DEFAULT_SET_DELTA_NOPURGED = false;
constexpr bool DEFAULT_SET_DELTA_NUM = false;
constexpr coefficient DEFAULT_SET_DELTA = 0.0;
constexpr coefficient DEFAULT_ACCURACY = 0.01;
constexpr coefficient DEFAULT_MAX_W0 = 1.0;
constexpr coefficient DEFAULT_MAX_DELTA = 10.0;
constexpr coefficient DEFAULT_MIN_FACTORS = -10.0;
constexpr coefficient DEFAULT_MAX_FACTORS = 10.0;
constexpr Method DEFAULT_METHOD = NNLR;
constexpr Model DEFAULT_MODEL = IP;
constexpr size_t DEFAULT_NGENEDROP = 1000000;
constexpr int DEFAULT_NBEES = 250;
constexpr int DEFAULT_LIMIT = 10;
constexpr size_t DEFAULT_NRUNS = 1;


// ====   CLASSES   ====

class Settings {

	/* PRIVATE ATTRIBUTES */
	protected:

	// Attributes in settings file
	time_t seed; // seed for pseudorandom number generation
	bool set_w0_noinbred; // estimate w0 from individuals with F=Fa=0?
	bool set_w0_num; // is w0 entered as a numeric value?
	coefficient set_w0;
	bool set_delta_nopurged; // estimate delta from non-purged individuals?
	bool set_delta_num; // is delta entered as numeric value?
	coefficient set_delta; // if delta is numeric, this is the value stored
	bool fixed_d; // fixed purging coefficient
	bool use_maternal; // true to add maternal effects
	bool use_factors; // true to add factors
	iVector factors; // columns of numeric factors to be used (from the input file)
	iVector nominal; // columns of categorical (nominal) factors t be used
	sVector fnames; // the names of the numerical factors (excluding factors in Fa-based models)
	sVector nnames; // names of categorical (nominal) factors
	sMatrix categories; // matrix of sorted levels for non-additional factors
	coefficient accuracy; // accuracy for estimates
	coefficient max_w0; // maximum value of initial fitness in ABC search
	coefficient max_delta; // maximum value of delta in ABC search
	Range range_factors; // searching range of factors in ABC method.
	bool save_log; // save a log file
	name log; // log file
	bool save_db; // save database
	bool save_db_all; // save_db including all individuals

	Method method; // method to be run
	Model model; // genetic model to be used
	bool genedrop; // use genedrop to estimate inbreeding
	size_t n_genedrop; // number of gene dropping iterations
	bool fa_model; // model is Fa based
	int nbees; // number of bees in the ABC algorithm
	int limit; // limit number of iterations (of convergence) in ABC
	size_t nruns; // number of runs [option 2]

	// Attributes defined in the constructor
	Range d; // the purging coefficient
	coefficient store_fixed_d;
	Range w0; // the initial fitness
	Range delta; // the inbreeding load
	size_t Nfactors; // the number of factors

	/* PUBLIC METHODS */
	public:

	// Constructor
	Settings ( ); 

	// Get attributes
	time_t get_seed () const { return seed; }
	bool get_use_maternal () const { return use_maternal; }
	bool get_use_factors () const { return use_factors; }
	bool get_save_log () const { return save_log; }
	bool get_save_db () const { return save_db; }
	bool get_save_db_all () const { return save_db_all; }
	Range get_d () const { return d; }
	bool is_d_fixed () const { return fixed_d; }
	coefficient get_fixed_d () const { return store_fixed_d; }
	Range get_w0 () const { return w0; }
	Range get_delta () const { return delta; }
	Range get_range_factors () const { return range_factors; }
	coefficient get_accuracy () const { return accuracy; }
	coefficient get_max_w0 () const { return max_w0; }
	coefficient get_max_delta () const { return max_delta; }
	iVector get_factors () const { return factors; }
	iVector get_nominal () const { return nominal; }
	sVector get_fnames () const { return fnames; }
	sVector get_nnames () const { return nnames; }
	sMatrix get_categories () const { return categories; }
	bool is_w0_num () const { return set_w0_num; }
	bool is_w0_noinbred () const { return set_w0_noinbred; }
	bool is_w0_b () const { return (!set_w0_num & !set_w0_noinbred); }
	coefficient get_w0_num () const { return set_w0; }
	bool is_delta_num () const { return set_delta_num; }
	bool is_delta_nopurged () const { return set_delta_nopurged; }
	coefficient get_delta_num () const { return set_delta; }
	name get_log () const { return log; }
	Method get_method () const { return method; }
	Model get_model () const { return model; }
	bool get_genedrop () const { return genedrop; }
	size_t get_n_genedrop () const { return n_genedrop; }
	bool get_fa_model () const { return fa_model; }
	int get_nbees () const { return nbees; }
	int get_limit () const { return limit; }
	size_t get_nruns () const { return nruns; }

	// Set attributes
	void set_model ( Model m ) { model = m; }
	void set_d () { d.min = 0.0; d.max = 0.5; d.step = accuracy; }
	void set_d ( Range v ) { d = v; }
	void set_d ( coefficient v ) {
		d.min  = v;
		d.max  = v;
		d.step = accuracy;
	}
	void restore_d () { // restore a fixed value of d
		if (!fixed_d) return;
		d.min  = store_fixed_d;
		d.max  = store_fixed_d;
		d.step = 0.0;
	}
	void set_num_delta ( coefficient d ) {
		set_delta_num = true;
		set_delta_nopurged = false;
		set_delta = d;
	}
	void set_use_factors ( bool v ) { use_factors = v; }
	void set_use_maternal ( bool v ) { use_maternal = v; }
	void add_factor ( size_t f, name n) {
		factors.push_back(f);
		fnames.push_back(n);
		++Nfactors;
	}
	void set_factors ( iVector f , sVector n ) {
		factors = f;
		fnames = n;
	}
	void set_categories ( sMatrix c ) {
		categories = c;
	}

	// Update settings
	void read_arguments ( int , char *[] );
	void read_settings ( name );
	void check ();

};

void print_help ( bool );

#endif
