#ifndef ABC_H
#define ABC_H

// =====   HEADER FILE INCLUDES   =====

#include <random> // std::mt19937 and std::uniform_real_distribution
#include "statistic.h"
#include "settings.h"


// =====   INITIALIZED VARIABLES  =====

extern time_t SEED;

extern std::mt19937 random_generator; // Mersenne Twister pseudorandom number generator engine


// ====   INLINE FUNCTIONS   ====

/* =======================================================================================
 * Function new_coord ()
 * ---------------------------------------------------------------------------------------
 *
 ======================================================================================= */
inline coefficient new_coord (coefficient Xik, coefficient Phi, coefficient Xij) {
	return Xik + Phi * ( Xik - Xij );
}


// ====   CLASSES   ====

/* Class Field represents the (hyper)space of variables and posible values to explore */
class Field {

	/* PRIVATE */
	protected:
	Vector X_de;  // Space of values for purguing coefficient (de)
	Vector X_w0;  // Space of values for initial fitness (w0)
	Vector X_delta;  // Space of values for inbreeding depression rate (delta)
	Vector X_factor;  // Space of values for additional factors
	size_t N_factor;  // Number of additional factors

	/* PUBLIC */
	public:
	Field ( coefficient , size_t , const Settings& );
	Field ( const Field& );

	Vector get_de_dimension () const;
	void set_de_dimension (const Vector& vector_de);
	Vector get_w0_dimension () const;
	coefficient get_w0 () const;
	Vector get_delta_dimension () const;
	coefficient get_delta () const;
	Vector get_factor_dimension () const;
	size_t get_factor_N () const;
	void reset_factors ();
	void rm_fa_factors ( Model );
	bVector variable_dimensions () const;
	bool known_w0 () const;
	bool known_delta () const;

};

/* Class Flower represents an individual coordinate in the space of all posible sources */
class Flower {

	/* PRIVATE */
	protected:
	coefficient d;  // purging coefficient
	coefficient w0;  // initial fitness
	coefficient delta;  // inbreeding depression rate
	Vector effects;  // aditional effects (the regression coefficient of other factors)

	/* PUBLIC */
	public:
	Flower ( coefficient , coefficient, coefficient , Vector );
	Flower ( Field );
	Nectar nectar ( const Vector& , const pVector& , const pMatrix& , coefficient );
};

/* Class Hive represents a swarm intelligence that looks for and keep in memory the best sources */
class Hive {

	/* PRIVATE */
	protected:
	// ABC specific variables
	size_t N;
	iVector coords;
	std::vector <Flower*> source;
	bVector memory;
	std::vector<Nectar> Mem;

	// extern variables
	Vector fitness;
	pVector G;
	pMatrix factors;
	coefficient accuracy;

	// convergence variables
	Nectar best_honey;
	Vector longterm_Mem;
	int long_Mem_ind;

	/* PUBLIC */
	public:
	Hive ( size_t , Field , const Vector& , const pVector& , const pMatrix& , int , coefficient );
	Nectar best_source ( );
	Nectar get_best_source ();
	bool converges ( int );
	void employers ( const Field&);
	void onlookers ( );
	void scouts ( Field );
	~Hive ();
};

// ====   FUNCTION'S  PROTOTYPES   ====

bool compare_RSS (const Nectar& , const Nectar& );
Nectar nectar_mean (const std::vector<Nectar>& );
Nectar nectar_sd (const std::vector<Nectar>& , Nectar );
coefficient nectar_mean_AIC (std::vector<Nectar> , size_t , size_t , Model );
coefficient nectar_sd_AIC (std::vector<Nectar> , coefficient , size_t , size_t , Model);

#endif
