#ifndef MAIN_H
#define MAIN_H

// =====   HEADER FILE INCLUDES   =====

#include "methods.h"


// =======   GLOBAL  VARIABLES  =======
name FILE_NAME; // input filename, without path or extension
name MM_FLAG; // a flag that indicates the method and model used
std::mt19937 random_generator; // Mersenne Twister pseudorandom number generator engine
bool VERBOSE;

#endif
