/* =======================================================================================
 *                                                                                       *.
 *    Filename:    statistic.cpp                                                         *
 *                                                                                       *
 *    Description: It contains functions that let calculate some parameters of statistic *
 *                 interest and other numerical functions.                               *
 *                                                                                       *
 * 				   -> mean ()                                                            *
 *                 -> sd ()                                                              *
 *                 -> sum ()                                                             *
 *                 -> min ()                                                             *
 * 				   -> SS ()                                                              *
 * 				   -> AIC ()                                                             *
 *                 -> RL_AIC ()                                                          *
 *                 -> wAIC ()                                                            *
 *                 -> CHI2 ()                                                            *
 *                 -> p_valCHI2 ()                                                       *
 *                 -> ifg_chi2 ()                                                        *
 *                                                                                       *
 ======================================================================================= */

#include "statistic.h"


/* =======================================================================================
 * Function mean ()
 * ---------------------------------------------------------------------------------------
 * Calculates the arithmetic mean of a given vector.
 * =====================================================================================*/
coefficient mean (const Vector& vector) {
	coefficient sum(0.0);
	size_t length(vector.size());
	for (size_t i(0); i<length; ++i) sum += vector[i]; 
	return (sum / length);
}

/* ------------------------------------------------------------------------------------ */
coefficient mean (const Vector& vector, bVector eval) {
	coefficient sum(0.0);
	size_t cont(0);
	size_t length(vector.size());
	for (size_t i(0); i<length; ++i) { if(eval[i]) { sum += vector[i]; ++cont; }} 
	return (sum / cont);
}

 /* =======================================================================================
 * Function sd ()
 * ---------------------------------------------------------------------------------------
 * Calculates the (population) standard deviation of a given vector.
 * =====================================================================================*/
coefficient sd (const Vector& vector) {
	 coefficient V (0.0);
	 size_t length (vector.size());
	 coefficient average (mean(vector));
	 for (size_t i(0); i< length; ++i) {
		 V += (vector[i]-average)*(vector[i]-average);
	 }
	 return sqrt(V/length);
 }

/* ------------------------------------------------------------------------------------ */
coefficient sd (const Vector& vector, bVector eval) {
	 coefficient V (0.0);
	 size_t cont (0) ;
	 size_t length (vector.size());
	 coefficient average (mean(vector,eval));
	 for (size_t i(0); i< length; ++i) {
		 if (eval[i]) {
			V += (vector[i]-average)*(vector[i]-average);
			++cont;
		}
	 }
	 return sqrt(V/cont);
 }

 /* =======================================================================================
 * Function sum ()
 * ---------------------------------------------------------------------------------------
 * Calculates the sum of the elements in a given vector.
 * =====================================================================================*/
coefficient sum (const Vector& vector) {
	 coefficient result (0);
	 for (size_t i(0); i<vector.size(); ++i) result += vector[i];
	 return result;
}

/* ------------------------------------------------------------------------------------ */
coefficient sum (const Vector& vector, bVector eval) {
	 coefficient result (0);
	 for (size_t i(0); i<vector.size(); ++i) if(eval[i]) result += vector[i];
	 return result;
}

/* ------------------------------------------------------------------------------------ */
size_t sum (const bVector& vector) {
	size_t result (0);
	 for (size_t i(0); i<vector.size(); ++i) if (vector[i]) ++result;
	 return result;
}

/* ------------------------------------------------------------------------------------ */
size_t sum (const bVector& vector, bVector eval) {
	 size_t result (0);
	 for (size_t i(0); i<vector.size(); ++i) if(eval[i] && vector[i]) ++result;
	 return result;
}

/* =======================================================================================
 * Function min ()
 * ---------------------------------------------------------------------------------------
 * Returns the minimum value in a vector.
 * =====================================================================================*/
coefficient min (const Vector& v) {
	coefficient min = v[0];
	for (std::size_t i(1); i<v.size(); ++i) if (v[i]<min) min = v[i];
	return min;
}

/* =======================================================================================
 * Function SS ()
 * ---------------------------------------------------------------------------------------
 * Calculates the total sum of squares (SS) of a vector of data about the mean:
 *     
 *     SS = SUM (Xi - X)^2
 *
 * =====================================================================================*/
coefficient SS (const Vector& vector) {
	coefficient SS(0.0), media (mean(vector));
	for (size_t i(0), length(vector.size()); i<length; ++i) SS += pow(vector[i]-media,2.0);
	return SS;
}

/* ------------------------------------------------------------------------------------ */
coefficient SS (const Vector& vector, bVector eval) {
	coefficient SS(0.0), media (mean(vector,eval));
	for (size_t i(0), length(vector.size()); i<length; ++i) { if (eval[i]) { SS += pow(vector[i]-media,2.0); }}
	return SS;
}

/* =======================================================================================
 * Function AIC ()
 * ---------------------------------------------------------------------------------------
 * Calculates Akaike's information criterion in the case of least squares (LS) regression.
 * AIC is defined for maximum likelihood regression as AIC = -2·log [L(model)] + 2·k.
 * Since log(L) = -(n/2)·log(2·pi·e·VAR), AIC can be calculated for least squares regression.
 * A corrected version includes a term that should be added always unless n/k > 40. Variance
 * is added to the k estimated parameters.
 * 
 *     AIC =  n·log[2·pi·e·VAR] + 2·k
 *     AICc = AIC + 2k(k+1) / (n-k-1) = n·log[2·pi·e·VAR] + (2kn) / (n-k-1)
 *
 * IMPORTANT: See Brian R. Mitchell Lectures. 
 * =====================================================================================*/
coefficient AIC (coefficient RSS, size_t n, size_t k, bool correction, Model model) {
	++k; // variance is added
	if (model == IP) ++k; // estimate of d

	if (correction) return n * log (Z*(RSS/n)) + (2.0*k*n) / (n-k-1); // 2.0 * k + (2.0*k*(k+1)) / (n-k-1);
	else            return n * log (Z*(RSS/n)) +  2.0*k;
}

/* =======================================================================================
 * Function RL_AIC ()
 * ---------------------------------------------------------------------------------------
 * Calculates the relative likelihood from AIC values. Given two models, with AIC1 and AIC2,
 * being AIC1 <= AIC2, this ratio is calculated as (Burnham et al 2002):
 *
 * 		RL = exp (0.5 * (AIC1-AIC2))
 *
 * This gives an indication on how many times the model with minimum AIC fits better the
 * data than the alternative (the relative likelihood of model with AIC2 with respect to
 * model with AIC1).
 * =====================================================================================*/
coefficient RL_AIC (coefficient AIC1, coefficient AIC2) {
	coefficient diff (AIC1-AIC2);
	if (AIC2 < AIC1) diff = AIC2-AIC1;
	return (exp(0.5*diff));
}

/* ------------------------------------------------------------------------------------ */
coefficient RL_AIC (const Vector& AICs, coefficient AICi) {
	coefficient AICmin (min(AICs));
	coefficient diff (AICmin-AICi);
	return (exp(0.5*diff));
}

/* =======================================================================================
 * Function w_AIC ()
 * ---------------------------------------------------------------------------------------
 * Calculates the weighted AIC, as the quotient between its corresponding relative likelihood
 * value and the sum of that of all considered models (two in this software).
 * =====================================================================================*/
coefficient w_AIC (coefficient test, coefficient alt) {
	coefficient RL = RL_AIC (test,alt);
	if (test < alt) return (1.0 / (1.0 + RL));
	else return (RL / (1.0 + RL));
}

/* =======================================================================================
 * Function CHI2 ()
 * ---------------------------------------------------------------------------------------
 * Calculates the CHI2 statistic from AIC values as:
 *
 * CHI2 = [ (AIC(simple model) - 2K_sm) – (AIC(complete model) – 2Kcm)]
 *
 * It has one degree of freedom if the complete model has only one parameter more than the
 * simpler one
 * =====================================================================================*/
coefficient CHI2 (coefficient AICcm, coefficient AICsm) {
	return (AICsm-AICcm+1.0);
}

/* ------------------------------------------------------------------------------------ */
coefficient p_valCHI2 ( coefficient AICcm, coefficient AICsm) {
	coefficient pvalue (-9.0);
	coefficient Dof (1.0); // in our case Dof always is 1
	coefficient Cv (CHI2(AICcm, AICsm));
	if (Cv <= 0.0 || Dof < 1.0) return 1.0;

	coefficient K = Dof * 0.5;
	coefficient X = Cv * 0.5;
	if(Dof == 2) return exp (-1.0 * X);

	pvalue = igf_chi2(K, X);
    if (std::isnan(pvalue) || std::isinf(pvalue) || pvalue <= 1e-8) return 1e-8;

    //pvalue /= gamma_chi2(K);
    pvalue /= tgamma(K);

	return (1.0 - pvalue);

}

/* ------------------------------------------------------------------------------------ */
coefficient igf_chi2 (coefficient S, coefficient Z) {
	// incomplete gamma function to compute chi2 pvalue
	// see https://en.wikipedia.org/wiki/Chi-squared_distribution#Cumulative_distribution_function
	if(Z < 0.0) return 0.0;
	coefficient Sc = (1.0 / S);
	Sc *= pow(Z, S);
	Sc *= exp(-Z);

	coefficient Sum = 1.0;
	coefficient Nom = 1.0;
	coefficient Denom = 1.0;

    for(int I = 0; I < 200; I++) {
		Nom *= Z;
		S++;
		Denom *= S;
		Sum += (Nom / Denom);
	}

	return Sum * Sc;
}
