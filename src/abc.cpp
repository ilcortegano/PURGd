/* =======================================================================================
 *                                                                                       *
 *       Filename:  abc.cpp                                                              *
 *                                                                                       *
 *    Description: This file contaings classes and methods for using Artificial Bee      *
 * 			       Colony (ABC) algorithm.                                               *
 *                                                                                       *
 *                 Refer to Karaboga's work (2007) to know more about ABC algorithm.     *
 *                                                                                       *
 *                  -> Field::Field ()                                                   *
 *                  -> Field::get_de_dimension ()                                        *
 *                  -> Field::set_de_dimension ()                                        *
 *                  -> Field::get_w0_dimension ()                                        *
 *                  -> Field::get_w0 ()                                                  *
 *                  -> Field::get_delta_dimension ()                                     *
 *                  -> Field::get_delta ()                                               *
 *                  -> Field::get_factor_dimension ()                                    *
 *                  -> Field::get_factor_N ()                                            *
 *                  -> Field::reset_factors ()                                           *
 *                  -> Field::variable_dimensions ()                                     *
 *                  -> Field::known_w0 ()                                                *
 *                  -> Field::known_delta ()                                             *
 *                                                                                       *
 * 					-> Flower::Flower ()                                                 *
 *                  -> Flower::nectar ()                                                 *
 *                                                                                       *
 *                  -> Hive::Hive ()                                                     *
 *                  -> Hive::employers ()                                                *
 *                  -> Hive::onlookers ()                                                *
 *                  -> Hive::scouts ()                                                   *
 *                  -> Hive::best_source ()                                              *
 *                  -> Hive::convergence ()                                              *
 *                  -> Hive::~Hive ()                                                    *
 * 
 *                  -> compare_RSS ()                                                    *
 *                  -> nectar_mean ()                                                    *
 *                  -> nectar_sd ()                                                      *
 *                  -> nectar_mean_AIC ()                                                *
 *                  -> nectar_sd_AIC ()                                                  *
 *                                                                                       *
 ======================================================================================= */

#include "abc.h"


/* ********************************************************************************************
 * Field::Field
 * --------------------------------------------------------------------------------------------
 * Class Field constructor.
 * ********************************************************************************************/
Field::Field (coefficient w0, size_t Nfactors, const Settings& settings)    {

	/* Range for purging coefficient (de) */
	X_de = range_to_vector (settings.get_d().min, settings.get_d().step, settings.get_d().max);

	/* Range for initial fitness (w0) */
	if (settings.is_w0_num() | settings.is_w0_noinbred()) {
		X_w0.push_back(w0);
	} else {
		X_w0 = range_to_vector (settings.get_w0().min, settings.get_w0().step, settings.get_w0().max);
	}

	/* Range for inbreeding depression rate (delta) */
	if (settings.is_delta_num()) X_delta.push_back(settings.get_delta_num());
	else X_delta = range_to_vector (settings.get_delta().min, settings.get_delta().step, settings.get_delta().max);

	/* Range for additional factors */
	if (Nfactors) X_factor = range_to_vector (settings.get_range_factors().min, settings.get_range_factors().step, settings.get_range_factors().max);
	N_factor = Nfactors                                                                                                                             ;

}

/* ------------------------------------------------------------------------------------------ */
Field::Field (
	const Field & field  ) :
	X_de (field.X_de), X_w0(field.X_w0), X_delta(field.X_delta), X_factor(field.X_factor), N_factor(field.N_factor)
	{ }

/* ********************************************************************************************
 * Field::get_de_dimension ()
 * --------------------------------------------------------------------------------------------
 * Returns vector of all possible values at "de" dimension.
 * ********************************************************************************************/
Vector Field::get_de_dimension () const {
	return X_de;
}

/* ********************************************************************************************
 * Field::set_de_dimension ()
 * --------------------------------------------------------------------------------------------
 * Sets vector of all possible values at "de" dimension.
 * ********************************************************************************************/
void Field::set_de_dimension (const Vector& vector_de) {
	X_de = vector_de;
}

/* ********************************************************************************************
 * Field::get_w0_dimension ()
 * --------------------------------------------------------------------------------------------
 * Returns vector of all possible values at "w0" dimension.
 * ********************************************************************************************/
Vector Field::get_w0_dimension () const {
	return X_w0;
}

/* ********************************************************************************************
 * Field::get_w0 ()
 * --------------------------------------------------------------------------------------------
 * This function should be called only if w0 is known. Returns the first value in w0 dimension
 * ********************************************************************************************/
coefficient Field::get_w0 () const {
	return X_w0[0];
}

/* ********************************************************************************************
 * Field::get_delta_dimension ()
 * --------------------------------------------------------------------------------------------
 * Returns vector of all possible values at "delta" dimension.
 * ********************************************************************************************/
Vector Field::get_delta_dimension () const {
	return X_delta;
}

/* ********************************************************************************************
 * Field::get_delta ()
 * --------------------------------------------------------------------------------------------
 * This function should be called only if delta is known. Returns the first value in delta
 * dimension
 * ********************************************************************************************/
coefficient Field::get_delta () const {
	return X_delta[0];
}

/* ********************************************************************************************
 * Field::get_factor_dimension ()
 * --------------------------------------------------------------------------------------------
 * Returns vector of all possible values at "additional factor" dimension.
 * ********************************************************************************************/
Vector Field::get_factor_dimension () const {
	return X_factor;
}

/* ********************************************************************************************
 * Field::get_factor_N ()
 * --------------------------------------------------------------------------------------------
 * Returns the number of additional factors.
 * ********************************************************************************************/
size_t Field::get_factor_N () const {
	return N_factor;
}

/* ********************************************************************************************
 * Field::reset_factors ()
 * --------------------------------------------------------------------------------------------
 * Remove the "additional factors" dimension.
 * ********************************************************************************************/
void Field::reset_factors () {
	X_factor.clear();
	N_factor = 0    ;
}

/* ********************************************************************************************
 * Field::rm_fa_factors ()
 * --------------------------------------------------------------------------------------------
 * Remove dimensions related to Fa factors.
 * ********************************************************************************************/
void Field::rm_fa_factors (Model model) {
	--N_factor;
	if (model==MX) --N_factor;
	if (!N_factor) {
		X_factor.clear();
	}
}

/* ********************************************************************************************
 * Field::variable_dimensions ()
 * --------------------------------------------------------------------------------------------
 * Returns the number of variable dimensions.
 * ********************************************************************************************/
bVector Field::variable_dimensions () const {
	bVector variation;

	if (X_de.size()>1) variation.push_back(true); else variation.push_back(false); // d dimension
	if (X_w0.size()>1) variation.push_back(true); else variation.push_back(false); // w0 dimension
	if (X_delta.size()>1) variation.push_back(true); else variation.push_back(false); // delta dimension
	for (size_t i(0); i<N_factor; ++i) {
		if (X_factor.size()>1) variation.push_back(true); else variation.push_back(false); // factor dimension: depends on N_factor and size
	}
	return variation;
}

/* ********************************************************************************************
 * Field::known_w0 ()
 * --------------------------------------------------------------------------------------------
 * Returns true if  the value of w0 is already known
 * ********************************************************************************************/
bool Field::known_w0 () const {
	return (X_w0.size() == 0);
}

/* ********************************************************************************************
 * Field::known_delta ()
 * --------------------------------------------------------------------------------------------
 * Returns true if  the value of delta is already known
 * ********************************************************************************************/
bool Field::known_delta () const {
	return (X_delta.size() == 0);
}

/* ********************************************************************************************
 * Flower::Flower
 * --------------------------------------------------------------------------------------------
 * Class flower constructor.
 * ********************************************************************************************/
Flower::Flower (
	coefficient d_    ,
	coefficient w0_   ,
	coefficient delta_,
	Vector effects_   ) {

	d = d_            ;
	w0 = w0_          ;
	delta = delta_    ;
	effects = effects_;
}

/* ------------------------------------------------------------------------------------------ */
Flower::Flower (Field field) {
	d = sample (field.get_de_dimension());
	if (field.known_w0())    w0 = field.get_w0(); else w0  = sample (field.get_w0_dimension());
	if (field.known_delta()) delta = field.get_delta(); else delta = sample (field.get_delta_dimension());

	for (size_t j(0); j < field.get_factor_N(); ++j) { effects.push_back(sample(field.get_factor_dimension())); }
}

 /* ********************************************************************************************
 * Flower::nectar
 * ---------------------------------------------------------------------------------------------
 * Returns a structure Nectar with information about flower coordinates, and the value of the
 * statistic being measured at the corresponding point.
 * ********************************************************************************************/
Nectar Flower::nectar (const Vector& fitness, const pVector& G, const pMatrix& pfactors, coefficient accuracy)   {

	size_t k (effects.size()); // number of additional factors
	size_t k2(pfactors.size()); // number of pointed additional factor matrix
	size_t N (fitness.size());

	/* Export flower coordenates */
	Nectar nectar_in_source;
	nectar_in_source.d = d;
	nectar_in_source.w0.value = w0;
	nectar_in_source.delta.value = delta;
	nectar_in_source.reg_coefficients = create_parameters(effects);

	/* Search the appropiate g vector for d */
	int coordinate = (int) (d/accuracy);
	if (G.size()==1) coordinate = 0;
	Vector g = *G[ coordinate ];

	Matrix factors;
	if (k && k2 == 1) factors = *pfactors[ 0 ];
	else if (k && k2 >  1) factors = *pfactors[ coordinate ];

	/* Calculate nectar value */
	Vector v_RSS;
	if (k) {
		for (size_t i(0); i<N; ++i) {
			coefficient add_factors (0.0);
			for (size_t j(0); j<k; ++j) {
				add_factors += effects[j] * factors[i][j];
			}
			v_RSS.push_back(pow (fitness[i] - ( w0 * exp (-delta * g[i] + add_factors )) ,2.0));
		}
	} else {
		for (size_t i(0); i<N; ++i) {
			v_RSS.push_back(pow (fitness[i] - ( w0 * exp (-delta * g[i] )) ,2.0));
		}
	}

	/* Export flower nectar value */
	nectar_in_source.stats.v_RSS = v_RSS;
	nectar_in_source.stats.RSS = sum(v_RSS);

	/* Save the matrix of factors used */
	nectar_in_source.factors = factors;

	return nectar_in_source;
}

/* ********************************************************************************************
 * Hive::Hive
 * --------------------------------------------------------------------------------------------
 * Class Hive constructor.
 * ********************************************************************************************/
Hive::Hive (size_t N_, Field field,	const Vector& fitness_ , const pVector& G_, const pMatrix& factors_, int limit,	coefficient accuracy_) {

	N = N_;
	memory = bVector (N,true);
	fitness = fitness_;
	G = G_;
	factors = factors_;
	accuracy = accuracy_;

	for (size_t i(0); i<N; ++i)	{
		source.push_back(new Flower(field));
		Mem.push_back(source[i]->nectar(fitness,G,factors,accuracy));
		coords.push_back(i);
	}

	longterm_Mem.resize(limit);
	long_Mem_ind = 0;

}

/* ********************************************************************************************
 * Hive::employers ()
 * --------------------------------------------------------------------------------------------
 * Employer bees evaluate nectar at every memory possition, and look for new sources in the
 * neighborhood. The best found source is then kept in th memory of the hive.
 * ********************************************************************************************/
void Hive::employers (const Field& field) {

	/* LOCAL VARIABLES */
	coefficient min_Phi (-1.0), max_Phi (1.0);
	Vector v_Phi (range_to_vector(min_Phi,accuracy,max_Phi));
	bVector variation (field.variable_dimensions());
	if (!any(variation)) return;
	memory = bVector (N,true);
	coefficient max_factor (0.0);
	iVector var_dim;

	coefficient max_w0 (field.get_w0_dimension()[field.get_w0_dimension().size()-1]);
	coefficient max_delta (field.get_delta_dimension()[field.get_delta_dimension().size()-1]);

	for (size_t i(0); i<variation.size(); ++i) if (variation[i]) var_dim.push_back(i)                         ;
	if (field.get_factor_N()) max_factor = field.get_factor_dimension()[field.get_factor_dimension().size()-1];

	for (size_t i(0); i < N; ++i ) {

		/* EMPLOYER BEE MOVES ON ONE RANDOM DIMENSION */
		size_t dimension (sample (var_dim)); coefficient min, max;

		switch (dimension) {
			case 0 : min = 0.0; max = 0.5; break;
			case 1 : min = field.get_w0 (); max = max_w0; break;
			case 2 : min = field.get_delta (); max = max_delta; break;
			default: min = field.get_factor_dimension()[0]; max = max_factor; break;
		}

		/* EMPLOYER BEE LOOKS FOR A NEW SOURCE NEARBY */
		coefficient near_d (Mem[i].d);
		coefficient near_w0 (Mem[i].w0.value);
		coefficient near_delta (Mem[i].delta.value);
		Vector near_factor (get_parameter_values(Mem[i].reg_coefficients));
		coefficient parameter;
		iVector N_j (coords); N_j.erase(N_j.begin()+i);

		do {
			coefficient Phi (sample(v_Phi)); size_t X_f (dimension-3);
			size_t j (sample(N_j));	Nectar random_source = Mem[j];

			switch (dimension) {
				case 0 : near_d = new_coord (Mem[i].d,Phi,random_source.d);
				parameter = near_d; break;
				case 1 : near_w0 = new_coord (Mem[i].w0.value,Phi,random_source.w0.value);
				parameter = near_w0; break;
				case 2 : near_delta = new_coord (Mem[i].delta.value,Phi,random_source.delta.value);
				parameter = near_delta; break;
				default: near_factor[X_f] = new_coord (Mem[i].reg_coefficients[X_f].value,Phi,random_source.reg_coefficients[X_f].value);
				parameter = near_factor[X_f]; break;
			}

		} while (parameter < min || parameter > max);

		/* EMPLOYER BEE EVALUATES A THE NEW SOURCE */
		Flower near_flower (near_d,near_w0,near_delta,near_factor);
		Nectar new_source (near_flower.nectar(fitness,G,factors,accuracy)) ;

		/* BEST SOURCE IS SELECTED BY EMPLOYER BEE */
		if (new_source.stats.RSS < Mem[i].stats.RSS) {
			delete source[i];
			source[i] = new Flower (near_flower);
			Mem[i] = new_source;
		}
	}
}

/* ********************************************************************************************
 * Hive::onlookers ()
 * --------------------------------------------------------------------------------------------
 * Onlooker bees select the best sources from between all available.
 * ********************************************************************************************/
void Hive::onlookers () {

	std::vector<coefficient> v_RSS; // como buscamos mínimos, los valores obtenidos de RSS serán inversos, tanto los individuales como la suma de ellos.
	coefficient sum_RSS (0.0);
	memory = bVector (N,false);
	std::size_t best_cord (0);
	coefficient best_RSS (Mem[0].stats.RSS);

	/* READS INFORMATION ON SOURCES*/
	for (size_t i(0); i < N; ++i) {
		if (Mem[i].stats.RSS < best_RSS) {
			best_RSS = Mem[i].stats.RSS;
			best_cord = i;
		}
		coefficient tmp_RSS (1.0/Mem[i].stats.RSS);
		sum_RSS += tmp_RSS;
		v_RSS.push_back(tmp_RSS);
	}

	/* A PROBABILITY IS GIVEN TO EACH FLOWER */
	coefficient tmp_RSS (0.0);
	if (sum_RSS > 0.0) {
		for (size_t i(0); i < N; ++i) {
			if (v_RSS[i] > 0.0) {
				v_RSS[i] = (v_RSS[i] / sum_RSS) + tmp_RSS;
				tmp_RSS = v_RSS[i];
	}	}	}

	/* ONLOOKER BEES DECIDE THE N BEST SOURCES */
	std::uniform_real_distribution<coefficient> dist(0.0, 1.0);

//	Note that dist will produce random numbers on the interval [0.0, 1.0)
//	According to cppreference, in order to sample over the closed interval [0.0, 1.0] we should use:
//	std::uniform_real_distribution<coefficient> dist(0.0, std::nextafter(1.0, std::numeric_limits<coefficient>::max()));
//	However, the results may depend too much on the implementation. Some people over stackoverflow discourage its use
//	Using the half-closed interval function should be enough for our purposes

	for (size_t i(0); i< N; ++i) {

		coefficient random = dist(random_generator);
		size_t coord(0);

		for (size_t j(0); j < N; ++j) {
			if (random <= v_RSS[j] && v_RSS[j] > 0.0) {
				coord = j; break;
			}
		}
		memory[coord] = true;
	}

	// The best source is always kept in memory
	memory[best_cord] = true;
}

/* ********************************************************************************************
 * Hive::scouts ()
 * --------------------------------------------------------------------------------------------
 * A number of scout bees look for new and random food sources.
 * ********************************************************************************************/
void Hive::scouts (Field field) {
	for (size_t i(0); i < N; ++i) {
		if (!memory[i]) {
			delete source[i];
			source[i] = new Flower (field);
			Mem[i] = source[i]->nectar(fitness,G,factors,accuracy);
		}
	}
}

/* ********************************************************************************************
 * Hive::best_source ()
 * --------------------------------------------------------------------------------------------
 * This function looks for the best food source in memory.
 * ********************************************************************************************/
Nectar Hive::best_source ( ){

	/* FIND DE BEST SOURCE */
	sort (Mem.begin(), Mem.end(), compare_RSS);
	best_honey = Mem[0];

	/* ROUND THE PURGING COEFFICIENT ESTIMATE */
	int coordinate ( (int) ( best_honey.d / accuracy));
	best_honey.d = coordinate * accuracy;

	return best_honey;
}

/* ********************************************************************************************
 * Hive::get_best_source ()
 * --------------------------------------------------------------------------------------------
 * Returns the best source found by the Hive
 * ********************************************************************************************/
Nectar Hive::get_best_source ( ){
	return best_honey;
}

/* ********************************************************************************************
 * Hive::convergence ()
 * --------------------------------------------------------------------------------------------
 * Checks for convergence of nectar values. This is assumed when best nectar does not change for
 * N consecutive iterations.
 * ********************************************************************************************/
bool Hive::converges (int limit) {

	bool convergence (false);
	longterm_Mem[long_Mem_ind] = best_honey.stats.RSS;
	++long_Mem_ind;
	if (!variation(longterm_Mem)) convergence = true;
	if (long_Mem_ind == limit) long_Mem_ind = 0;

	return convergence;
}

/* ********************************************************************************************
 * Hive::~Hive
 * --------------------------------------------------------------------------------------------
 * Class Hive destructor.
 * ********************************************************************************************/
Hive::~Hive () {
	for (size_t i(0); i<N; ++i) delete source[i];
	source.clear();
}

/* ********************************************************************************************
 * compare_RSS ()
 * --------------------------------------------------------------------------------------------
 * True if first RSS is lower than te second.
 * ********************************************************************************************/
bool compare_RSS (const Nectar& nectar1, const Nectar& nectar2) {
	return nectar1.stats.RSS < nectar2.stats.RSS;
}

/* ********************************************************************************************
 * nectar_mean ()
 * --------------------------------------------------------------------------------------------
 * It takes a vector of mean, and returns the averaged nectar structure
 * ********************************************************************************************/
Nectar nectar_mean (const std::vector<Nectar>& vector) {

	Nectar average_nectar (vector[0]);
	size_t N(vector.size());

	for (size_t i(1); i<N; ++i) {

		average_nectar.d += vector[i].d                                                                 ;
		average_nectar.w0.value += vector[i].w0.value;
		average_nectar.delta.value += vector[i].delta.value;
		average_nectar.stats.RSS += vector[i].stats.RSS;
		for (size_t j(0); j<vector[i].reg_coefficients.size(); ++j) average_nectar.reg_coefficients[j].value += vector[i].reg_coefficients[j].value;
		for (size_t j(0); j<vector[i].stats.v_RSS.size(); ++j) average_nectar.stats.v_RSS[j] += vector[i].stats.v_RSS[j];

	}

	average_nectar.d /= N;
	average_nectar.w0.value /= N;
	average_nectar.delta.value /= N;
	average_nectar.stats.RSS /= N;
	for (size_t i(0); i<average_nectar.reg_coefficients.size(); ++i) average_nectar.reg_coefficients[i].value /= N;
	for (size_t i(0); i<average_nectar.stats.v_RSS.size(); ++i) average_nectar.stats.v_RSS[i] /= N;

	return average_nectar;
}

/* ********************************************************************************************
 * nectar_sd ()
 * --------------------------------------------------------------------------------------------
 * It takes a vector of mean, and returns a nectar structure of standard deviations
 * ********************************************************************************************/
Nectar nectar_sd (const std::vector<Nectar>& vector, Nectar averaged_nectar) {

	Nectar sd_nectar;
	sd_nectar.d = 0.0;
	sd_nectar.w0.value = 0.0;
	sd_nectar.delta.value = 0.0;
	sd_nectar.stats.RSS = 0.0;
	sd_nectar.reg_coefficients = create_parameters (Vector (averaged_nectar.reg_coefficients.size(),0.0));
	for (size_t i(0); i<averaged_nectar.stats.v_RSS.size(); ++i) sd_nectar.stats.v_RSS.push_back(0.0);
	size_t N(vector.size());

	// Compute variance
	for (size_t i(0); i<N; ++i) {
		sd_nectar.d += ((averaged_nectar.d - vector[i].d) * (averaged_nectar.d - vector[i].d));
		sd_nectar.w0.value += ((averaged_nectar.w0.value - vector[i].w0.value) * (averaged_nectar.w0.value - vector[i].w0.value));
		sd_nectar.delta.value += ((averaged_nectar.delta.value - vector[i].delta.value) * (averaged_nectar.delta.value - vector[i].delta.value));
		sd_nectar.stats.RSS += ((averaged_nectar.stats.RSS - vector[i].stats.RSS) * (averaged_nectar.stats.RSS - vector[i].stats.RSS));
		for (size_t j(0); j<vector[i].reg_coefficients.size(); ++j) sd_nectar.reg_coefficients[j].value += ((averaged_nectar.reg_coefficients[j].value - vector[i].reg_coefficients[j].value) * (averaged_nectar.reg_coefficients[j].value - vector[i].reg_coefficients[j].value));
		for (size_t j(0); j<vector[i].stats.v_RSS.size()  ; ++j) sd_nectar.stats.v_RSS[j] += ((averaged_nectar.stats.v_RSS[j] - vector[i].stats.v_RSS[j]) * (averaged_nectar.stats.v_RSS[j] - vector[i].stats.v_RSS[j]));
	}

	if (sd_nectar.d > 0.0) sd_nectar.d = sd_nectar.d/N;
	if (sd_nectar.w0.value > 0.0) sd_nectar.w0.value = sd_nectar.w0.value/N;
	if (sd_nectar.delta.value > 0.0) sd_nectar.delta.value = sd_nectar.delta.value/N;
	if (sd_nectar.stats.RSS > 0.0) sd_nectar.stats.RSS = sd_nectar.stats.RSS/N;
	for (size_t i(0); i<sd_nectar.reg_coefficients.size(); ++i) if (sd_nectar.reg_coefficients[i].value > 0.0) sd_nectar.reg_coefficients[i].value = sd_nectar.reg_coefficients[i].value/N;
	for (size_t i(0); i<sd_nectar.stats.v_RSS.size(); ++i) if (sd_nectar.stats.v_RSS[i] > 0.0) sd_nectar.stats.v_RSS[i] = sd_nectar.stats.v_RSS[i]/N;

	// Compute standard deviations
	if (sd_nectar.d > 0.0) sd_nectar.d = sqrt(sd_nectar.d/N);
	if (sd_nectar.w0.value > 0.0) sd_nectar.w0.value = sqrt(sd_nectar.w0.value/N);
	if (sd_nectar.delta.value > 0.0) sd_nectar.delta.value = sqrt(sd_nectar.delta.value/N);
	if (sd_nectar.stats.RSS > 0.0) sd_nectar.stats.RSS = sqrt(sd_nectar.stats.RSS/N);
	for (size_t i(0); i<sd_nectar.reg_coefficients.size(); ++i) if (sd_nectar.reg_coefficients[i].value > 0.0) sd_nectar.reg_coefficients[i].value = sqrt(sd_nectar.reg_coefficients[i].value/N);
	for (size_t i(0); i<sd_nectar.stats.v_RSS.size(); ++i) if (sd_nectar.stats.v_RSS[i] > 0.0) sd_nectar.stats.v_RSS[i] = sqrt(sd_nectar.stats.v_RSS[i]/N);

	return sd_nectar;
}

/* ********************************************************************************************
 * nectar_mean_AIC ()
 * --------------------------------------------------------------------------------------------
 * CIt takes a vector of mean, and returns a nectar structure of standar deviations
 * ********************************************************************************************/
coefficient nectar_mean_AIC (std::vector<Nectar> vector, size_t n, size_t k, Model model) {
	coefficient AICc (0.0) ;
	size_t N(vector.size());
	for (size_t i(0); i<N; ++i) {
		AICc += AIC(vector[i].stats.RSS,n,k,true,model);
	}

	return AICc/N;
}

/* ********************************************************************************************
 * nectar_sd_AIC ()
 * --------------------------------------------------------------------------------------------
 * CIt takes a vector of mean, and returns a nectar structure of standar deviations
 * ********************************************************************************************/
coefficient nectar_sd_AIC (std::vector<Nectar> vector, coefficient averaged_AIC, size_t n, size_t k, Model model) {
	coefficient V (0.0) ;
	size_t N(vector.size());
	for (size_t i(0); i<N; ++i) {
		V += (averaged_AIC - AIC(vector[i].stats.RSS,n,k,true,model)) * (averaged_AIC - AIC(vector[i].stats.RSS,n,k,true,model));
	}

	return sqrt(V/N);
}
