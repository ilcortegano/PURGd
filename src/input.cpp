/* =======================================================================================
 *                                                                                       *
 *       Filename:  input.cpp                                                            *
 *                                                                                       *
 *    Description:  This file contains functions for read, check and work with input     *
 * 					files.                                                               *
 *                                                                                       * 
 *                  -> input_Smatrix ()                                                  *
 *                  -> get_path ()                                                       *
 *                  -> get_filename ()                                                   *
 *                                                                                       *
 ======================================================================================= */

#include "input.h"


/* =======================================================================================
 * Function input_Smatrix ()
 * ---------------------------------------------------------------------------------------
 * This function reads text files and stores them in a matrix of type std::string. The
 * returned matrix can contain a header or not.
 ======================================================================================= */
sMatrix input_Smatrix (name filename, bool return_header) {

	name value;
	sMatrix array;
	int cont (0);

	name data (filename);
	std::ifstream file (data);

	if (file.is_open() ) {

		while ( getline (file, value) ) {

			if (value!="") {
				sVector ROW;
				std::istringstream iss (value);
				name result;
				while (getline(iss, result, ',')) {
					if (result.size() && result[result.size()-1] == '\r') result = result.substr( 0, result.size() - 1 );
					if (!result.size()) throw_error(ERROR_EMPT);
					ROW.push_back(result.c_str());
				}
				if (cont || return_header) array.push_back(ROW);
			}
			++cont;
		}
		file.close();

	} else {
		pequal(pe,'='); std::cerr << ERROR_INPT << " " << filename << "\n";
		pequal(pe,'='); exit(-1);
	}

	return array;
}

/* =======================================================================================
 * Function get_path ()
 * ---------------------------------------------------------------------------------------
 * Assumes that PATH are a string and folders are '/' delimited. It uses split_vector()
 * to get the full string from the start to the last '/'.
 * =====================================================================================*/
name get_path (name string) {
	std::string path ("");
	sVector full_path (split_vector(string,'/'));
	for (size_t i(0); i<full_path.size()-1; ++i) path = path + full_path[i] + '/';
	return path;
}

/* =======================================================================================
 * Function get_filename ()
 * ---------------------------------------------------------------------------------------
 * Takes an string and takes the name in between the last '/' delimiter and the first '.'
 * extension sign.
 * =====================================================================================*/
name get_filename (name string) {
	std::string filename ("");
	sVector path (split_vector(string,'/'));
	filename = path[path.size()-1];
	return (split_vector(filename,'.'))[0];
}
