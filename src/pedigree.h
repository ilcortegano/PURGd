#ifndef DEMOGRAPHY_H
#define DEMOGRAPHY_H


// =====   HEADER FILE INCLUDES   =====

#include "input.h"
#include "settings.h"
#include "inbreeding.h"
#include "genedrop.h"
#include "statistic.h"


// =====   INITIALIZED VARIABLES  =====

const name null_id ("0");


// FUNCTION TEMPLATES

/* =======================================================================================
 * Function founder ()
 * ---------------------------------------------------------------------------------------
 * Returns true if the parent of individual in row i (dame or sire) is a founder in the pedigree.
 * =====================================================================================*/
template<typename T>
bool founder (const std::vector<std::vector<T>>& pedigree, size_t gender, size_t i) {
	for (size_t j(0); j<i; ++j)	if (pedigree[i][gender] == pedigree[j][icol]) return false;
	return true;
}

template<typename T>
void repeated_ids (std::vector<T> vector1) {

	int N(vector1.size());
	for (int i(1); i<N; ++i) {
		for (int j(i-1); j>=0; --j) if (vector1[j]==vector1[i]) {
			pequal(pe,'='); std::cerr << ERROR_REPT << vector1[j] << " (row " << j+1 << ") and " << vector1[i] << " (row " << i+1 << ").\n";
			pequal(pe,'='); exit(-1)                                                                                                                     ;
		}
	}
}


// ====   CLASSES   ====
struct DB { // pedigree (summary) database
	sVector ids;
	Vector w;
	Vector F;
	coefficient d;
	Vector Fa;
	Vector Fag;
	sMatrix factors;
};

class Pedigree {
	
	/* PRIVATE ATTRIBUTES */
	protected:
	name pedigree_name; // name of the pedigree file
	size_t N; // number of individuals
	sVector ids; // vector of IDs (first column)
	iMatrix pedigree; // pedigree matrix
	Vector w_col; // original vector of fitness (fourth column)
	Vector fitness; // vector of fitness
	Parameter w0; // fitness for non-inbred individuals
	sVector fa_names; // name of Fa-based models factors
	sMatrix m_sfactors; // (complete) matrix of effect of additional factors on each individual.
	Matrix m_factors; // numeric matix of additional factors
	sMatrix categories; // matrix of sorted levels for non-additional factors
	Matrix dummies; // matrix of categorical factors casted into dummy variables
	Inbreeding inbreeding;
	Vector F; // Wright's inbreeding coefficient
	pVector g; // vector of pointers to purged inbreeding coefficient vectors
	Vector d; // vector of purging coefficient values
	Vector Fa; // Ancestral inbreeding coefficient (Ballou's or genedrop)
	Vector ballou_Fa; // If genedropping is used to estimate Fa, this vector saves Ballou's Fa
	Vector Ff; // Product F·Fa
	Vector Fd; // Maternal inbreeding
	pVector gd; // vector of pointers to maternal purged inbreeding
	bVector eval; // Vector of individuals to evaluate (NAs are coded as 0)
	DB database; // db vectors

	/* PUBLIC METHODS */
	public:
	Pedigree ( name );
	void update (Settings& );
	~Pedigree ();

	name get_name () const { return get_filename(pedigree_name); }
	sVector get_IDs () const { return ids; }
	iMatrix get_pedigree () const { return pedigree; }
	Vector get_wcol () const { return w_col; }
	Vector get_fitness () const { return fitness; }
	bVector get_eval () const { return eval; }
	sMatrix get_m_sfactors () const { return m_sfactors; }
	Matrix get_m_factors () const { return m_factors; }
	Vector get_F () const { return F; }
	Vector get_Fa () const { return Fa; }
	Vector get_ballou_Fa () const { return ballou_Fa; }
	Vector get_Ff () const { return Ff; }
	pVector get_g () const { return g; }
	Vector get_d () const { return d; }
	pVector get_gd () const { return gd; }
	Parameter get_w0 () const { return w0; }
	DB read_db () const { return database; }
	Vector calc_g (coefficient di) { return inbreeding.compute_purging(pedigree, di); }
	Vector calc_mat (Vector v) { return inbreeding.m_inbreeding (pedigree, v);}
	pMatrix matrix_of_factors ( const Settings& , Model );

	protected:
	void update_eval ( sVector );
	void update_db ();
};

// ====   FUNCTION'S  PROTOTYPES   ====
iMatrix read_pedigree ( const sMatrix& );
sMatrix read_spedigree ( const sMatrix& );
coefficient RSS_fitness ( const Vector& , const Vector& , coefficient );
void base_eval ( bVector& , const Vector& , const Vector& );
void slog ( Vector& , bVector& );

#endif
