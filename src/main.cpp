/* =======================================================================================
 *                                                                                       *
 *       Filename:  main.cpp                                                             *
 *                                                                                       *
 *    Description:  PURGd is a software to detect purging and to estimate inbreeding-    *
 *                  purging genetic parameters in pedigreed populations.                 *                                                           *
 *                                                                                       *
 *                  This file initializes the program reading ordinary pedigree files    *
 *                  and calling different methods (only NNLR by now).                    *
 *                                                                                       *
 *                  Please, refer to the user manual to know more about input and output *
 *                  files, and how the program works.                                    *
 *                                                                                       *
 *                                                                                       *
 *        Version:  2.2.0                                                                *
 *        Created:  01/04/2014                                                           *
 *        Updated:  03/03/2020                                                           *
 *       Compiler:  g++ (9.2.1)                                                          *
 *          Flags:  -Wall -c "%f" -std=c++11                                             *
 *                  -Wall -o "%e" "%f" -lgsl -lgslcblas -std=c++11                       *
 *                                                                                       *
 *         Author:  Eugenio López-Cortegano                                              *
 *          Email:  e.lopez-cortegano@ed.ac.uk                                           *
 *   Organization:  University of Edinburgh                                              *
 *                                                                                       *
 ======================================================================================= */

#include "main.h"


int main ( int argc , char *argv[] ) {

	/* READ SETTINGS FILE */
	Settings settings;
	settings.read_arguments (argc,argv);
	settings.check();

	/* LOCAL VARIABLES */
	name pedigree_file (argv[argc-1]);
	Method method (settings.get_method());
	FILE_NAME = get_filename (pedigree_file);
	name PATH (get_path(pedigree_file));

	/* ESTIMATION METHODS */
	random_generator.seed(settings.get_seed()); // seed the generator

	/* NON LINEAR NUMERICAL METHOD */
	if (method == NNLR) nnlr_method (pedigree_file, settings);
	else throw_error (ERROR_UNKM);

	/* SAVE LOG */
	if (settings.get_save_log()) save_log (settings,FILE_NAME+MM_FLAG);

	/* THE END */
	pequal(pe,'='); std::cout << "Finish!" << std::endl; pequal(pe,'=');
	return 0;
}
