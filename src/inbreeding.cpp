/* =======================================================================================
 *                                                                                       *
 *    Filename:    inbreeding.cpp                                                        *
 *                                                                                       *
 *    Description: Here are defined functions involving neutral, purged and ancestral    *
 *                 inbreeding.                                                           *
 *                                                                                       *
 *                 -> init ()                                                            *
 * 				   -> define_t ()                                                        *
 *                 -> search_ancestors ()                                                *
 *                 -> minimum_coancestry ()                                              *
 *                 -> compute_purging ()                                                 *
 *                 -> a_inbreeding ()                                                    *
 *                 -> m_inbreeding ()                                                    *
 *                                                                                       *
 ======================================================================================= */
 
#include "inbreeding.h"


/* ********************************************************************************************
 * Inbreeding::init
 * --------------------------------------------------------------------------------------------
 * Initializes all parameters required for inbreeding calculations
 * ********************************************************************************************/
void Inbreeding::init (const iMatrix& pedigree) {
	N = pedigree.size();
	Npairs = 0;
	define_t(pedigree);
}

/* =======================================================================================
 * Function define_t ()
 * ---------------------------------------------------------------------------------------
 * Calculates genetation number for each individual, even if there are overlaped generations.
 * Generations start with t=0, and are assigned sequencially for individual i=0 to i=N-1 so
 * any individual belongs to the same generation than any of its parents.
 ======================================================================================= */
void Inbreeding::define_t (const iMatrix& pedigree) {
	size_t gen(0);
	for (size_t i(0); i<N; ++i) {
		if (pedigree[i][dcol] != 0 && t[pedigree[i][dcol]-1]==gen) ++gen;
		else if (pedigree[i][scol] != 0 && t[pedigree[i][scol]-1]==gen) ++gen;
		t.push_back(gen);
	}

	t_map[0] = 0;
	gen = 0;
	for (size_t i(1); i<N; ++i) {
		if (t[i] != t[i-1]) {
			++gen;
			t_map[gen] = i;
		}
	}
}

/* =======================================================================================
 * Function search_ancestors ()
 * ---------------------------------------------------------------------------------------
 * This function recursively search ancestors whose coancestry must be considered in order
 * to estimate the relationship betwen a given pair of individuals
 * =====================================================================================*/
void Inbreeding::search_ancestors (const iMatrix& pedigree, std::set<coord>& log, const std::size_t& i, const std::size_t& j) {
	if ((log.count(std::make_pair(i,j))!=0) || (log.count(std::make_pair(j,i))!=0) || !i || !j) return;
	if (i<j) log.insert(std::make_pair(i,j));
	else log.insert(std::make_pair(j,i));
	if (t[i-1]==t[j-1]) {
		search_ancestors (pedigree, log, pedigree[i-1][dcol], pedigree[j-1][dcol]);
		search_ancestors (pedigree, log, pedigree[i-1][dcol], pedigree[j-1][scol]);
		search_ancestors (pedigree, log, pedigree[i-1][scol], pedigree[j-1][dcol]);
		search_ancestors (pedigree, log, pedigree[i-1][scol], pedigree[j-1][scol]);
	} else if (t[i-1] < t[j-1]) {
		search_ancestors (pedigree, log, pedigree[i-1][icol], pedigree[j-1][dcol]);
		search_ancestors (pedigree, log, pedigree[i-1][icol], pedigree[j-1][scol]);
	} else {
		search_ancestors (pedigree, log, pedigree[j-1][icol], pedigree[i-1][dcol]);
		search_ancestors (pedigree, log, pedigree[j-1][icol], pedigree[i-1][scol]);
	}
}

/* =======================================================================================
 * Function minimum_coancestry ()
 * ---------------------------------------------------------------------------------------
 * Calculates a vector of inbreeding coefficient (F) using two-generation coancestry
 * matrix in each step, from parents to offspring. Thes index of the required minimum-
 * coancestry matrix are in a log vector
 * =====================================================================================*/
void Inbreeding::minimum_coancestry (const iMatrix& pedigree) {

	init(pedigree);

	/* Compute minimum required pairs */
	if (VERBOSE) {
		pequal(pe,'=');	std::cout << "\rBuilding minimum coancestry matrix... ";
		std::cout << std::flush;
	}

	for (int i(N-1); i>=0; --i) {
		search_ancestors (pedigree, required_pairs,pedigree[i][dcol],pedigree[i][scol]);
	}
	Npairs = required_pairs.size();

	/* Compute minimum coancestry matrix */
	coancestry_matrix f_map;
	f_map.coancestry.reserve (Npairs);
	for (std::set<coord>::iterator it=required_pairs.begin(); it!=required_pairs.end(); ++it) {
		std::size_t i (std::get<0>(*it));
		std::size_t j (std::get<1>(*it));
		if (i==j) {
			f_map[std::make_pair(i,j)] = 0.5 * (1.0 + f_map[std::make_pair(pedigree[i-1][dcol],pedigree[i-1][scol])]);
		} else if (t[i-1]==t[j-1]) {
			coefficient fAC (f_map[std::make_pair(pedigree[i-1][dcol], pedigree[j-1][dcol])]);
			coefficient fAD (f_map[std::make_pair(pedigree[i-1][dcol], pedigree[j-1][scol])]);
			coefficient fBC (f_map[std::make_pair(pedigree[i-1][scol], pedigree[j-1][dcol])]);
			coefficient fBD (f_map[std::make_pair(pedigree[i-1][scol], pedigree[j-1][scol])]);
			f_map[std::make_pair(i,j)] = 0.25 * (fAC + fAD + fBC + fBD);
		} else {
			coefficient fAE(0.0), fAH(0.0);
			fAE = f_map[std::make_pair(pedigree[i-1][icol], pedigree[j-1][dcol])];
			fAH = f_map[std::make_pair(pedigree[i-1][icol], pedigree[j-1][scol])];
			f_map[std::make_pair(i,j)] = 0.5  * (fAE + fAH );
		}
	}
	if (VERBOSE) std::cout << "\rBuilding minimum coancestry matrix...done!" << std::endl;

	/* Read inbreeding */
	Vector F;
	for (std::size_t i(0); i<N; ++i) F.push_back(f_map[std::make_pair(pedigree[i][dcol],pedigree[i][scol])]);
	inbreeding.push_back(new Vector (F));
}

/* =======================================================================================
 * Function compute_purging ()
 * ---------------------------------------------------------------------------------------
 * Uses the log map to fast computing purged inbreeding coancestry and inbreeding
 * =====================================================================================*/
Vector Inbreeding::compute_purging (const iMatrix& pedigree, const coefficient& d) {

	// Create a minimum coancestry matrix
	coancestry_matrix g_map;
	g_map.coancestry.reserve (Npairs);
	for (std::set<coord>::iterator it=required_pairs.begin(); it!=required_pairs.end(); ++it) {
		std::size_t i (std::get<0>(*it));
		std::size_t j (std::get<1>(*it));
		if (i==j) {
			g_map[std::make_pair(i,j)] = 0.5 * (1.0 + g_map[std::make_pair(pedigree[i-1][dcol],pedigree[i-1][scol])]) * (1.0 - 2.0*d*(*inbreeding[0])[i-1]);
		} else if (t[i-1]==t[j-1]) {
			coefficient fAC (g_map[std::make_pair(pedigree[i-1][dcol], pedigree[j-1][dcol])]);
			coefficient fAD (g_map[std::make_pair(pedigree[i-1][dcol], pedigree[j-1][scol])]);
			coefficient fBC (g_map[std::make_pair(pedigree[i-1][scol], pedigree[j-1][dcol])]);
			coefficient fBD (g_map[std::make_pair(pedigree[i-1][scol], pedigree[j-1][scol])]);
			g_map[std::make_pair(i,j)] = 0.25 * (fAC + fAD + fBC + fBD) * (1.0 - d*((*inbreeding[0])[i-1]+(*inbreeding[0])[j-1]));
		} else {
			coefficient fAE(0.0), fAH(0.0);
			fAE = g_map[std::make_pair(pedigree[i-1][icol], pedigree[j-1][dcol])];
			fAH = g_map[std::make_pair(pedigree[i-1][icol], pedigree[j-1][scol])];
			g_map[std::make_pair(i,j)] = 0.5  * (fAE + fAH) * (1.0 - d*(*inbreeding[0])[j-1]);
		}
	}

	Vector g;
	for (std::size_t i(0); i<N; ++i) g.push_back(g_map[std::make_pair(pedigree[i][dcol],pedigree[i][scol])]);
	return g;
}

/* =======================================================================================
 * Function a_inbreeding ()
 * ---------------------------------------------------------------------------------------
 * Calculates ancestral inbreeding coefficient as in Ballou, J.D. (1997).
 * =====================================================================================*/
void Inbreeding::calc_Fa (const iMatrix& pedigree) {
	for (size_t i(0); i<N; ++i) {

		/* LOCAL VARIABLES */
		size_t dame (pedigree[i][dcol]);
		size_t sir  (pedigree[i][scol]);
		coefficient f_dame(0.0), fa_dame(0.0);
		coefficient f_sir (0.0), fa_sir (0.0);

		/* PARENTS NEUTRAL AND ANCESTRAL INBREEDING*/
		if (dame) { f_dame = (*inbreeding[0])[dame-1]; fa_dame = Fa[dame-1];} // F = (*inbreeding[0])
		if (sir ) { f_sir  = (*inbreeding[0])[sir -1]; fa_sir  = Fa[sir -1];}
		
		/* ANCESTRAL INBREEDING*/
		Fa.push_back( 0.5*( fa_dame + (1.0-fa_dame)*f_dame + fa_sir + (1.0-fa_sir)*f_sir ) );
	}
	Ff = (*inbreeding[0]) * Fa;
}

/* =======================================================================================
 * Function m_inbreeding ()
 * ---------------------------------------------------------------------------------------
 * Calculates an individual's mother (purged) inbreeding coefficient.
 * =====================================================================================*/
Vector Inbreeding::m_inbreeding (const iMatrix& PED, const Vector& f) {
	Vector fd;
	for (size_t i(0); i<N; ++i) {
		if (PED[i][dcol]!=0) fd.push_back( f[ PED[i][dcol] -1 ] );
		else fd.push_back(0.0);
	}
	return fd;
}
