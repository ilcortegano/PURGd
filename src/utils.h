#ifndef UTILS_H
#define UTILS_H

// =====   HEADER FILE INCLUDES   =====

#include <sstream> // isstringstream
#include <string.h> // strspn
#include "alias.h"


// =====   FUNCTION'S PROTOTYPES   =====

bool string_to_bool ( const name& );
void kill_pVector ( pVector& );
void kill_pMatrix ( pMatrix& );
name plimit ( coefficient );
bool isNumeric ( const name& );
bool any ( const bVector& );
name tr_model ( Model );
sVector split_vector ( name , char );
sVector split_vector ( sVector , char );

#endif
