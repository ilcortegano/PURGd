#ifndef ERROR_H
#define ERROR_H


// =====   HEADER FILE INCLUDES   =====

#include <iostream>


// =====   INITIALIZED VARIABLES  =====

constexpr short	pe (96); // number of signs printed by pequal()


// =========   ERROR MESSAGES  ========
const std::string

	// Errors reading options from the terminal
	ERROR_PSIN = "Wrong syntax. Use option ' --help ' for usage info",
	ERROR_PINP = "Please enter a valid input file or enter 'PURGd --help'",
	ERROR_CONF = "Wrong syntax for --config option. Please, enter --config=<FILE>",
	ERROR_DUPL = "Presence of duplicated or incompatible input options",

	// Errors reading settings file
	ERROR_SETF = "Failed to input settings file",
	ERROR_SREA = "Error reading variables in settings file",
	ERROR_FORM = "Wrong format in settings file",
	ERROR_OUTF = "Output path parameter must have double quotation in settings file",
	ERROR_LOGF = "Custom log filenames must have double quotation in settings file",

	// Errors in setting parameters values
	ERROR_SEED = "Invalid seed. It must be a positive integer",
	ERROR_METH = "Wrong method. It must be 'NNLR'",
	ERROR_PMOD = "Wrong purging model. It must be IP, BW, BA or MX",
	ERROR_USEW = "Error setting initial fitness parameter",
	ERROR_USEB = "Error setting inbreeding load parameter",
	ERROR_ACCU = "Accuracy value out of range (0.0, 0.5]",
	ERROR_DRAN = "Purging coefficient value out of range [0.0, 0.5]",
	ERROR_FIXD = "Cannot set purging coefficient for no IP models",
	ERROR_MAXW = "The maximum value for initial fitness must be a positive number",
	ERROR_MAXB = "The maximum value for inbreeding load must be a positive number",
	ERROR_RANF = "Invalid range values for additional factors",
	ERROR_USEM = "Maternal effects parameter is not \"0\" or \"1\"",
	ERROR_SVDB = "Save databases parameter is not \"0\", \"1\", or \"ALL\"",
	ERROR_NBEE = "Minimum number of bees is 250",
	ERROR_LIMT = "Minimum number of iterations of convergence is 10",
	ERROR_NRUN = "Number of runs must be positive",
	ERROR_NGEN = "Number of gene dropping iterations must be positive",
	ERROR_MGEN = "Gene dropping iterations limited to 10e9",
	ERROR_LOGN = "The log filename should not include a path ('/') character",
	ERROR_PATH = "Cannot write output files: check if the custom output path exists",

	// NNLR-settings errors
	ERROR_NOLR = "Linear regression (LR) method is deprecated, please use version 2.0 or previous releases",
	ERROR_UNKM = "Unknown method",
	ERROR_NNLR_BEE = "Number of bees only defined for ABC algorithm (in NNLR method)",
	ERROR_NNLR_LIM = "Limit number of iterations is only defined for ABC algorithm (in NNLR method)",
	ERROR_NNLR_MW0 = "Maximum value of initial fitness parameter only defined for NNLR method",
	ERROR_NNLR_MDT = "Maximum value of the inbreeding depression rate only defined for NNLR method",
	ERROR_NNLR_AFT = "Range values for additional factors only defined for NNLR method",
	ERROR_NNLR_RUN = "Number of runs only defined for NNLR method",

	// Only IP-model errors
	ERROR_NOIP_MTD = "The method for estimating purged inbreeding is only defined for the IP model",
	ERROR_NOIP_FAD = "Cannot use a known inbreeding depression rate (delta) value for Fa-based models",
	ERROR_NOIP_FAS = "Cannot estimate the inbreeding depression rate for Fa-based models. Use IP instead",

	// Estimate delta with Fa=0

	// Additional factors errors
	ERROR_USEF_ODD = "Additional factors format is wrong: column number or factor name missing?",
	ERROR_USEF_ARG = "Additional factors option set, but none specified",
	ERROR_USEF_REP = "Repeated column number or name for additional factors",
	ERROR_USEF_MIN = "Additional factor column number must be higher than \"4\"",
	ERROR_USEF_MAX = "Cannot define more additional factors than additional columns in input file",
	ERROR_USEF_VAR = "Additional factor with no variation found",
	ERROR_USEF_OUT = "Additional factor column number out of actual number of columns",
	ERROR_USEF_RNG = "Cannot define a search range for additional factors with no factors",
	ERROR_USEF_NUM = "At least one numeric factor contains non-numeric characters",
	ERROR_USEF_NON = "Declared additional factors but columns are missing on input file",

	// Errors in pedigree file
	ERROR_NCOL = "Input file must have at least 4 columns",
	ERROR_VROW = "Rows in the input file differ in the number of columns",
	ERROR_EMPT = "Found empty values in the pedigree file",
	ERROR_INPT = "Incorrect name or unable to open the file: ",
	ERROR_ORDR = "Individuals are not sorted",
	ERROR_SELF = "And individual cannot be its own parent, can it?",
	ERROR_REPT = "At least two identities are identical: ",
	ERROR_FVAR = "No variation for inbreeding coefficient (F)",
	ERROR_MVAR = "No variation for maternal inbreeding (Fd)",
	ERROR_AVAR = "No variation for ancestral inbreeding (Fa)";


// ====   INLINE FUNCTIONS   ====

/* =======================================================================================
 * Function pequal ()
 * ---------------------------------------------------------------------------------------
 * Prints a selected character a variable number of times.
 ======================================================================================= */
inline void pequal (short n, char sign) {
	for (short i(0); i<n; ++i)	std::cout << sign;
	std::cout << std::endl;
}

/* =======================================================================================
 * Function throw_error ()
 * ---------------------------------------------------------------------------------------
 * Returns an error message, and exist the run of the program.
 ======================================================================================= */
inline void throw_error (std::string message) {
	pequal(pe,'='); std::cerr << "ERROR: " << message << ".\n"; pequal(pe,'='); exit(-1);
}

/* =======================================================================================
 * Function throw_error ()
 * ---------------------------------------------------------------------------------------
 * Returns an error message, and exist the run of the program.
 ======================================================================================= */
inline void throw_error (std::string message, std::string tag) {
	pequal(pe,'='); std::cerr << "ERROR: " << message << " [ " << tag << " ]\n"; pequal(pe,'='); exit(-1);
}


#endif
