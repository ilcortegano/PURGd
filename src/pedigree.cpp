/* =======================================================================================
 *                                                                                       *
 *    Filename:    pedigree.cpp                                                          *
 *                                                                                       *
 *    Description: Here are defined the methos of the class Pedigree. From reading       *
 *	  pedigree files, edite pedigrees and operate with their main attributes.            *
 *                                                                                       *
 *                 -> Pedigree::Pedigree ()                                              *
 *                 -> Pedigree::~Pedigree ()                                             *
 *                 -> Pedigree::update ()                                                *
 *                 -> Pedigree::update_eval ()                                           *
 *                 -> Pedigree::matrix_of_factors ()                                     *
 *                 -> Pedigree::get_* ()                                                 *
 *                                                                                       *
 *                 -> take_pedigree ()                                                   *
 *                 -> id ()                                                              *
 *                 -> RSS_fitness ()                                                     *
 *                 -> base_eval ()                                                       *
 *                 -> slog ()                                                            *
 *                                                                                       *
 ======================================================================================= */
 
#include "pedigree.h"


/* ********************************************************************************************
 * Pedigree::Pedigree
 * --------------------------------------------------------------------------------------------
 * Class Pedigree constructor.
 * It includes methods read the input pedigree file and define basic pedigree atributes from it.
 * ********************************************************************************************/
Pedigree::Pedigree (name filename) {

	pedigree_name = filename;

	/* INPUT PEDIGREE FILE */
	sMatrix full_data (input_Smatrix(pedigree_name, false));
	if (!check_min_col (full_data, 4)) throw_error(ERROR_NCOL); // check minimum of four columns
	if (check_var_row (full_data)) throw_error(ERROR_VROW); // check if all rows have the same columns

	/* GET VECTOR OF IDs */
	ids = column_to_vector (full_data, icol);
	repeated_ids (ids); // check repeated IDs
	N = ids.size();

	bool correct_pedigree (false); // true if pedigree IDs need correction (if IDs contain strings or are not ordered from 1 to N-1)
	for (int i(0); i<(int)N; ++i) {
		if ( !isNumeric(ids[i]) || ( atoi(ids[i].c_str()) != i+1 ) ) { correct_pedigree = true; break; }
	}

	/* GET & CHECK PEDIGREE */
	sMatrix test_pedigree = read_spedigree (full_data);
	// Check id order
	for (std::size_t i(0); i<N; ++i) {
		std::string test_id (test_pedigree[i][icol]);
		for (std::size_t j(0); j<i; ++j) {
			if (test_id==test_pedigree[j][dcol] || test_id==test_pedigree[j][scol]) {
				throw_error(ERROR_ORDR, "ID " + test_id + " is declared after descendant with ID " + test_pedigree[j][icol] + ".");
			}
		}
		if (test_id==test_pedigree[i][dcol] || test_id==test_pedigree[i][scol]) throw_error(ERROR_SELF, "check ID " + test_id + " with parents " + test_pedigree[i][dcol] + " and " + test_pedigree[i][scol] + ".");
	}

	if (!correct_pedigree) pedigree = read_pedigree (full_data);
	else {
		for (size_t i(0); i<N; ++i) {
			iVector row(1, i + 1);
			if (founder(test_pedigree,dcol,i)) row.push_back(0);
			else row.push_back(find(test_pedigree[i][dcol],ids) + 1);
			if (founder(test_pedigree,scol,i)) row.push_back(0);
			else row.push_back(find(test_pedigree[i][scol],ids) + 1);
			pedigree.push_back(row);
		}
	}
	test_pedigree.clear();

	/* GET FITNESS */
	for (size_t i(0); i<N; ++i) {

		// Fitness
		fitness.push_back(atof(full_data[i][wcol].c_str()));
		w_col = fitness;

		// Remaining columns are saved into effects matrix
		sVector row;
		for (size_t j(wcol+1); j<max(full_data); ++j) {
			row.push_back(full_data[i][j]);
		}
		m_sfactors.push_back(row);
	}

	/* OTHER PEDIGREE PARAMETERS */
	eval = bVector (N,true);
	this->update_eval(column_to_vector(full_data,wcol));

	/* INBREEDING CALCULATIONS */
	inbreeding.minimum_coancestry (pedigree);
	inbreeding.calc_Fa(pedigree);
	F = inbreeding.get_F();
	Fa = inbreeding.get_Fa();
	Ff = F * Fa;

	/* Record database */
	update_db();

}

/* ********************************************************************************************
 * Pedigree::update
 * --------------------------------------------------------------------------------------------
 * Modifies a pedigree from the information entered from command line (arguments) and settings.txt.
 * It also applies a filter of individuals to evaluate using <eval> vector
 * ********************************************************************************************/
void Pedigree::update (Settings& settings) {

	/* LOCAL VARIABLES */
	bool use_genedrop (settings.get_genedrop());
	bool use_maternal (settings.get_use_maternal());
	bool add_factors (settings.get_use_factors());
	iVector factors (settings.get_factors());
	sVector fnames (settings.get_fnames());
	iVector nominal (settings.get_nominal());
	sVector nnames (settings.get_nnames());
	Model model (settings.get_model());
	bool fa_model (settings.get_fa_model());

	/* RECALCULATE INBREEDING IF GENEDROP IS SELECTED */
	if (use_genedrop) {
		ballou_Fa = Fa;
		Fa = genedrop(pedigree, settings.get_n_genedrop());
		Ff = F * Fa;
	}

	/* Update database */
	if (settings.get_save_db() & !settings.get_save_db_all()) update_db();

	/* Calculate initial fitness */
	if (settings.is_w0_num()) {
		w0.value = settings.get_w0_num();
	} else if (settings.is_w0_noinbred()) {
		bVector tmp_eval (eval); base_eval (tmp_eval,F,Fa);
		w0.value = mean (fitness,tmp_eval);
		w0.ET = sd (fitness,tmp_eval) / sqrt(sum(tmp_eval));
	}

	/* Update eval vector with information from selected additional factors */
	if (add_factors && (m_sfactors[0].size() < factors.size())) throw_error (ERROR_USEF_NON);
	else if (factors.size()+nominal.size() > max (m_sfactors)) throw_error (ERROR_USEF_MAX);
	for (size_t i(0); i<factors.size(); ++i) {
		this->update_eval(column_to_vector(m_sfactors,factors[i]-4));
	}
	for (size_t i(0); i<nominal.size(); ++i) {
		this->update_eval(column_to_vector(m_sfactors,nominal[i]-4));
	}

	/* Estimate the inbreeding depression rate (using Fa=0 individuals) */
	if (settings.is_delta_nopurged()) {
		for (size_t i(0); i<eval.size(); ++i) { if (Fa[i]!=0.0) eval[i] = false; }
		model = ID;
		settings.set_model(ID);
		settings.set_d(0.0);
	} else settings.restore_d();

	/* If maternal effects are used, calculate maternal inbreeding */
	if (use_maternal) { Fd = inbreeding.m_inbreeding (pedigree,F); }

	/* Calculate purged inbreeding coefficient */
	if (model == IP) {

		// d coefficient
		coefficient d_coeff (settings.get_d().min);
		coefficient d_max (settings.get_d().max);
		coefficient d_acc (settings.get_d().step);
		int N_d ((int)((d_max - d_coeff)/d_acc) +1);

		if (d_coeff != d_max) {
			for (int i(0); i<N_d; ++i) {
				d.push_back(d_coeff);
				d_coeff += d_acc;
			}
		} else d.push_back(d_coeff);

		// g and gd
		if (VERBOSE) pequal(pe,'=');
		for (size_t i(0); i < d.size(); ++i) {

			Vector tmp_g;
			if (d[i]==0.0) tmp_g = F;
			else tmp_g = inbreeding.compute_purging(pedigree, d[i]);

			g.push_back( new Vector (correct_vector(tmp_g,eval)));
			if(use_maternal) {
				gd.push_back( new Vector (correct_vector(inbreeding.m_inbreeding (pedigree,tmp_g),eval)));
			}
			if (VERBOSE) {
				std::cout << "\rPre-computing purged inbreeding coefficients: " << i+1 << " / " << d.size();
				std::cout << std::flush;
			}
		}
		if (VERBOSE) std::cout << std::endl;
	} else {
		d.push_back(0.0);
		g.push_back( new Vector (correct_vector (F,eval)));
		if(use_maternal) gd.push_back( new Vector (correct_vector (Fd,eval)));
	}

	/* Get subset of numeric additional factors */
	if (add_factors && factors.size()) {

		// Subset matrix of effects
		for (size_t i(0); i<N; ++i) {
			Vector row;
			for (size_t j(0); j<factors.size(); ++j) {
				// Avoid factor column number higher than actual effects matrix
				if ( factors[j]-4 >= max(m_sfactors)) throw_error (ERROR_USEF_OUT);
				if (!isNumeric(m_sfactors[i][factors[j]-4].c_str()) && m_sfactors[i][factors[j]-4]!="NA") throw_error(ERROR_USEF_NUM);
				row.push_back(atof(m_sfactors[i][factors[j]-4].c_str())); // effects matrix will contain only columns in tmp_effects that are specified by settings factors
			}
			m_factors.push_back(row);
		}
	}

	/* Get subset of categorical additional factors */
	if (add_factors && nominal.size()) {

		sMatrix tmp_m_cfactors;
		// Subset matrix of effects
		for (size_t i(0); i<N; ++i) {
			sVector row;
			for (size_t j(0); j<nominal.size(); ++j) {
				// Avoid factor column number higher than actual effects matrix
				if ( nominal[j]-4 >= max(m_sfactors)) throw_error (ERROR_USEF_OUT);
				row.push_back(m_sfactors[i][nominal[j]-4]);
			}
			tmp_m_cfactors.push_back(row);
		}

		// Get factor levels
		for (size_t i(0); i<nominal.size(); ++i) {
			sVector levels;
			for (size_t j(0); j<N; ++j) {
				if ((!is_in(tmp_m_cfactors[j][i], levels)) && eval[j]) levels.push_back(tmp_m_cfactors[j][i]);
			}
			if (levels.size() <=1) throw_error (ERROR_USEF_VAR);
			std::sort(levels.begin(), levels.end(), std::less<std::string>());
			categories.push_back(levels);
		}
		settings.set_categories(categories);

		// Create dummy variables
		size_t start (0);
		if (settings.is_w0_b()) start = 1;
		for (size_t i(0); i<N; ++i) {
			Vector ind_levels;
			for (size_t j(0); j<categories.size(); ++j) {
				for (size_t k(start); k<categories[j].size(); ++k) {
					if (tmp_m_cfactors[i][j] == categories[j][k]) ind_levels.push_back(1.0);
					else ind_levels.push_back(0.0);
				}
			}
			dummies.push_back(ind_levels);
		}
	}

	/* MERGE NUMERIC AND CATEGORIC FACTORS */
	if (add_factors) {
		if (dummies.size() && !m_factors.size()) m_factors = dummies;
		else if (dummies.size() && m_factors.size()) cbind (m_factors, dummies);
	}

	/* Add factors for ancestral inbreedig models */
	if (fa_model) {
		if      (model == BA)   fa_names.push_back("Ballou");
		else if (model == BW)   fa_names.push_back("BAW")   ;
		else if (model == MX) {	fa_names.push_back("Ballou");fa_names.push_back("BAW"); }
	}

	/* Apply a filter using <eval> vector to remaining relveant vectors */
	fitness = correct_vector (fitness,eval);
	F = correct_vector (F,eval);
	Fa = correct_vector (Fa,eval);
	Ff = correct_vector (Ff,eval);
	Fd = correct_vector (Fd,eval);
	if (use_genedrop) correct_vector(ballou_Fa,eval);
	if (add_factors) {
		m_sfactors = rm_rows (m_sfactors,eval);
		m_factors  = rm_rows (m_factors,eval);
	}

	// Check variation in inbreeding coefficients and additional factors
	if (!variation(F)) {
		throw_error (ERROR_FVAR);
	}

	if (fa_model && !variation(Fa)) {
		throw_error (ERROR_AVAR);
	}

	if (use_maternal && !variation(Fd)) {
		throw_error (ERROR_MVAR);
	}

	if (add_factors) {
		if (factors.size()) {
			for (std::size_t i(0); i<factors.size(); ++i) {
				if (!variation(column_to_vector (m_sfactors,factors[i]-4))) {
					throw_error (ERROR_USEF_VAR, fnames[i]);
				}
			}
		}
		if (nominal.size()) {
			for (std::size_t i(0); i<nominal.size(); ++i) {
				if (!variation(column_to_vector (m_sfactors,nominal[i]-4))) {
					throw_error (ERROR_USEF_VAR, nnames[i]);
				}
			}
		}
	}

	// set more filters
	if (fitness.size() != g[0]->size() ) { 
		std::cerr << "class Pedigree: ERROR in data filtering" << std::endl; 
		exit(-1)                                                           ;
	}

}

/* =======================================================================================
 * Function update_eval ()
 * ---------------------------------------------------------------------------------------
 * Update eval vector removing "NA" values from a vector of names
 ======================================================================================= */
void Pedigree::update_eval (sVector vector) {
	
	if ( N != vector.size()) {
		std::cerr << "ERROR: diferent number of individuals in eval vector and control vector (update_eval())\n";
		exit(-1);
	}
	for (size_t i(0); i<N; ++i) {
		if (vector[i] == "NA") {
			eval[i] = false;
		}
	}
}

/* =======================================================================================
 * Function update_db ()
 * ---------------------------------------------------------------------------------------
 * Saves a copy of relevant parameters in a dedicated database structure
 ======================================================================================= */
void Pedigree::update_db () {
	database.ids = ids;
	database.w = fitness;
	database.F = F;
	database.d = 0.0;
	database.Fa = Fa;
	if (ballou_Fa.size()) {
		database.Fag = Fa;
		database.Fa = ballou_Fa;
	}
	database.factors = m_sfactors;
}

/* =======================================================================================
 * Function matrix_of_factors ()
 * ---------------------------------------------------------------------------------------
 * It takes factors based on Fa, other additional factors, and  maternal effects and puts
 * them together in a matrix.
 * =====================================================================================*/
pMatrix Pedigree::matrix_of_factors (const Settings& settings, Model run_model) {

	/* LOCAL VARIABLES */
	Model model (settings.get_model());
	bool fa_model (settings.get_fa_model()); // use Fa-based models
	bool run_fa_model (true);
	if (run_model == ID || run_model == IP) {
		run_fa_model = false;
	} else if (!fa_model) {
		throw_error ("Cannot force Fa-based analysis for a non Fa-based model [matrix_of_factors]");
	}
	bool maternal (settings.get_use_maternal());
	bool use_factors (settings.get_use_factors());
	size_t N_fa_factors (0);
	Matrix Fa_matrix; // matrix of Fa-based factors
	pMatrix M_factors; // vector of pointers to matrix of factors (returned)

	/* MATRIX OF FA-BASED FACTORS */	
	if (fa_model & run_fa_model) {
		++ N_fa_factors;
		if (model == BA) {
			Fa_matrix = vector_to_matrix (Ff);
		} else if (model == BW) {
			Fa_matrix = vector_to_matrix (Fa);
		} else if (model == MX) {	
			++ N_fa_factors;
			Fa_matrix = vector_to_matrix (Ff);
			add_column (Fa_matrix,Fa,1);
		}
	}

	/* MERGE FACTORS INTO A MATRIX POINTED BY [M_factors], FOLLOWING THE SCHEDULE:
	[fa = fa-based factors] + [gd = maternal effects] + [af = other additional factors]
	There will be as many matrix of additional factors as <gd> vectors (ie. <d> values) */

	if (fa_model & run_fa_model) {
		if (maternal) {
			// [fa] + [gd] + [af]
			if (use_factors) {
				for (size_t i(0); i<gd.size(); ++i) {
					Matrix tmp_factors (vector_to_matrix(*gd[i]));
					cbind (Fa_matrix,tmp_factors)                ;
					cbind (Fa_matrix,m_factors)                  ;
					M_factors.push_back( new Matrix (Fa_matrix)) ;
				}
			// [fa] + [gd] + [  ]
			} else {
				for (size_t i(0); i<gd.size(); ++i) {
					Matrix tmp_factors (vector_to_matrix(*gd[i]));
					cbind (Fa_matrix,tmp_factors)                ;
					M_factors.push_back( new Matrix (Fa_matrix)) ;
				}
			}
		} else {
			// [fa] + [  ] + [af]
			if (use_factors) {
				cbind (Fa_matrix, m_factors)               ;
				M_factors.push_back(new Matrix (Fa_matrix));
			// [fa] + [  ] + [  ]
			} else {
				M_factors.push_back(new Matrix (Fa_matrix));
			}			
		}
	} else {
		if (maternal) {
			// [  ] + [gd] + [af]
			if (use_factors) {
				for (size_t i(0); i<gd.size(); ++i) {
					Matrix tmp_factors (vector_to_matrix(*gd[i])) ;
					cbind (tmp_factors,m_factors)                 ;
					M_factors.push_back( new Matrix (tmp_factors));
				}
			// [  ] + [gd] + [  ]
			} else {
				for (size_t i(0); i<gd.size(); ++i) {
					M_factors.push_back( new Matrix (vector_to_matrix(*gd[i])));
				}
			}
		} else {
			// [  ] + [  ] + [af]
			if (use_factors) {
				M_factors.push_back(new Matrix (m_factors));
			// [  ] + [  ] + [  ]
			} else {}			
		}
	}

	return M_factors;
}

/* ********************************************************************************************
 * Pedigree::~Pedigree
 * --------------------------------------------------------------------------------------------
 * Class Pedigree destructor.
 * ********************************************************************************************/
Pedigree::~Pedigree () {
	kill_pVector (g) ; 
	kill_pVector (gd);
}

/* =======================================================================================
 * Function read_pedigree ()
 * ---------------------------------------------------------------------------------------
 * Takes the pedigree from the input matrix
 * =====================================================================================*/
iMatrix read_pedigree (const sMatrix& data) {
	iMatrix pedigree;
	for (size_t i(0), N(data.size()); i<N; ++i) {
		iVector row;
		row.push_back(atoi(data[i][icol].c_str()));
		row.push_back(atoi(data[i][dcol].c_str()));
		row.push_back(atoi(data[i][scol].c_str()));
		pedigree.push_back(row);
	}
	return pedigree;
}

/* ----------------------------------------------------------------------------------- */
sMatrix read_spedigree (const sMatrix& data) {
	sMatrix pedigree;
	for (size_t i(0), N(data.size()); i<N; ++i) {
		sVector row;
		row.push_back(data[i][icol]);
		row.push_back(data[i][dcol]);
		row.push_back(data[i][scol]);
		pedigree.push_back(row);
	}
	return pedigree;
}

/* =======================================================================================
 * Function RSS_fitness ()
 * ---------------------------------------------------------------------------------------
 * Calculates RSS between expected fitness value in log scale, and the observed ones, in
 * the regression method when the intercept is known.
 * =====================================================================================*/
coefficient RSS_fitness (const Vector& Y, const Vector& g, coefficient slope) {

	coefficient RSS (0.0);
	size_t N (Y.size());

	for (size_t i(0); i<N; ++i) {
		RSS += pow (Y[i] - slope*g[i],2.0);
	}

	return RSS;
}

/* =======================================================================================
 * Function base_eval ()
 * ---------------------------------------------------------------------------------------
 * 
 ======================================================================================= */
void base_eval (bVector& eval, const Vector& f, const Vector& fa) {
	size_t N (eval.size());
	for (size_t i(0); i<N; ++i) if (fa[i] != 0 || f[i] != 0 ) eval[i] = false;
}

/* =======================================================================================
 * Function slog ()
 * ---------------------------------------------------------------------------------------
 * This function applies log trasformation to each value in a numeric vector and returns 
 * the resultant vector.
 * =====================================================================================*/
void slog (Vector& fitness, bVector& eval) {
	size_t nrows(fitness.size());
	for (size_t i(0); i<nrows; ++i) {
		if (fitness[i] > 0.0) fitness[i] = log(fitness[i]);
		else eval[i] = false                              ;
	}
}
