#ifndef STATISTIC_H
#define STATISTIC_H

// =====   HEADER FILE INCLUDES   =====

#include <cmath>
#include "alias.h"
#include "error.h"


// =====   INITIALIZED VARIABLES  =====

constexpr coefficient alpha (0.05); // significance level
constexpr coefficient Z (17.07946844534713); // 2*pi*exp(1) in AIC formula
constexpr coefficient boot (10000.0); // number of bootstrap samples


// ====   FUNCTION'S  PROTOTYPES   ====

coefficient mean ( const Vector& );
coefficient mean ( const Vector& , bVector );
coefficient sd ( const Vector& );
coefficient sd ( const Vector& , bVector );
coefficient sum ( const Vector& );
coefficient sum ( const Vector& , bVector );
size_t sum ( const bVector& );
size_t sum ( const bVector& , bVector );
coefficient min ( const Vector& );
coefficient SS ( const Vector& );
coefficient SS ( const Vector& , bVector );
coefficient AIC ( coefficient , size_t , size_t , bool , Model );
coefficient RL_AIC ( coefficient , coefficient );
coefficient RL_AIC ( const Vector& , coefficient );
coefficient w_AIC ( coefficient , coefficient );
coefficient CHI2 ( coefficient , coefficient );
coefficient p_valCHI2 ( coefficient, coefficient );
coefficient igf_chi2 ( coefficient , coefficient );

#endif
