#ifndef INPUT_H
#define INPUT_H

// =====   HEADER FILE INCLUDES   =====

#include <fstream> // ifstream
#include <sstream> // isstringstream
#include "utils.h"
#include "error.h"
#include "templates.h"

// =======   GLOBAL  VARIABLES  =======
extern name FILE_NAME;
extern name MM_FLAG;


// =====   INITIALIZED VARIABLES  =====
const name CSV = ".csv";


// =====   FUNCTION'S PROTOTYPES   =====
sMatrix input_Smatrix ( name , bool );
name get_path ( name );
name get_filename ( name );

#endif
