# PURGd 2.2 <img src="man/images/icon35.png" /> [![build status](https://gitlab.com/elcortegano/PURGd/badges/master/pipeline.svg)](https://gitlab.com/elcortegano/PURGd/commits/master)
A software to detect purging and to estimate inbreeding-purging genetic parameters in pedigreed populations.

## Overview

PURGd is a software developed to detect purging and to estimate inbreeding-purging genetic (IP) parameters in pedigreed populations. The models and methods used in this software are described in García-Dorado *et al* (2016).

The main objective of this program is to estimate the effective purging coefficient (*d*) described by García-Dorado (2012), which is an overall genomic measure of the component of the deleterious effects that is only expressed in homozygosis and is therefore responsible for purging under inbreeding. Furthermore, the program also estimates the regression coefficients on the purged inbreeding coefficient (*g*) and on additional regressors, such as environmental factors or maternal inbreeding. This software also includes options to estimate parameters for purging models based on ancestral inbreeding, developed by Ballou (1997) and Boakes & Wang (2005).

This software was published in:

- García-Dorado A, Wang J, López-Cortegano E. (2016). *Predictive model and software for inbreeding-purging analysis of pedigreed populations*. G3: Genes, Genomes, Genetics 6 (11): 3593-3601.

Please, cite it if you use PURGd in your research.

Other references highlighted above are:

- Ballou JD. (1997). *Ancestral inbreeding only minimally affects inbreeding depression in mammalian populations*. Journal of Heredity 88: 169-178.

- Boakes E, Wang J. (2005). *A simulation study on detecting purging of inbreeding depression in captive populations*. Genetical Research 86: 139-148.

- García-Dorado A. (2012). *Understanding and predicting the fitness decline of shrink populations: inbreeding, purging, mutation and standard selection*. Genetics 190, 1461-1476.

## Quick install

You can download PURGd from the terminal by entering:

```
git clone https://gitlab.com/elcortegano/PURGd.git
```

It can be compiled from source by using `make`. See the Wiki to read more installation details.

```
cd src/
make
sudo make install
```

[Docker](https://www.docker.com/) is also available to download and safely run a PURGd docker image independently of the OS you use:

```
docker pull elcortegano/purgd:latest
```

Alternatively, a Windows binary is also provided in the release notes of this repository. Download the most recent one [here](https://gitlab.com/elcortegano/PURGd/uploads/02318b7c2a0007be09db42310708b7f5/PURGd.exe).

Refer the [wiki](https://gitlab.com/elcortegano/PURGd/-/wikis/Installation) to know more about the different installation options, as well as how to compile this software in Windows machines.

## User's guide

A complete User's guide is included as `man/manual.md`. Details on the software usage, as well as input, output and settings files used are included in there. This file can be read as a plain text file, but it can also be rendered to alternative formats (*e.g.* PDF) for convenience using [pandoc](http://pandoc.org/):

    pandoc manual.md -f markdown -t latex --pdf-engine=xelatex -s -o manual.pdf

The User's guide also includes a description of options that can be used to modify PURGd behaviour (*i.e.* genetic model, regression method, ...). These options may be entered from the terminal, but may also be read from a settings file, as described in the user's guide.

## Assistance

Please read the full User's guide (*i.e.* `man/manual.md`) file before running the program and/or asking any question about it.

If you have found a bug, want to request an additional feature, or don't understand an option about PURGd, please open a new issue (with the right tag and including as much information as possible) in the gitlab [issue tracker for PURGd](https://gitlab.com/elcortegano/PURGd/issues) (It will require an account).

## License

PURGd: A software to detect purging and to estimate inbreeding-purging genetic parameters in pedigreed populations.

Copyright (C) 2020  Eugenio López-Cortegano, Diego Bersabé, Jinliang Wang and Aurora García-Dorado.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
