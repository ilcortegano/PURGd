# CHANGELOG for PURGd

This file reflects the most important changes between releases. For further details, please chech the commits in the releases branches in gitlab.

## v2.2 (03/03/2020)
- Options for estimating initial fitness and inbreeding load have been renamed
- Code for saving output files has been simplified
- Minor changes and improvements on saved output files and printed summary
- Minor fixes when saving output files
- Improvements on CI/CD pipeline

## v2.1.5 (18/02/2020)
- Improvements and fixes in cpptools
- Added Rtools to compute Ne and h2

## v2.1.4 (15/12/2019)
- Added helper programs in cpptools
- Added Rmarkdown application to compute IP predictions in rtools

## v2.1.3 (12/12(2019)
- Fixed bug affecting output of Ballou's Fa in databases (not genedrop Fa)

## v2.1.2 (17/09/2019)
- Fixes checking variation on additional factors

## v2.1.1 (16/09/2019)
- Improvements on Docker image
- Improvements on CI/CD pipeline

## v2.1 (08/07/2019)
- LR method is deprecated.
- Improved cross-platform compatibility after removing external dependencies (#4).
- Changes in license.
- Minor bug fixes.

## v2.0 (03/07/2019)
- Higher performance for calculating inbreeding and relationship for large datasets.
- Enhanced the use of additional factors.
- Improvements in the statistics returned in the output files.
- Fixed in method to estimate delta from Fa=0 data.
- Added small improvements, features and bug fixes.
- Updated documentation.

## v1.4.4 (17/09/2018)
- Allow analyses using categorical variables as additional factors (issue #7).

## v1.4.3 (23/08/2018)
- Completed integration to Gitlab
- Added support to estimate w0 as regressor for LR method (issue #9).
- Fixed bug in v1.4.2 regarding calculation of non-purging model for NNLR.

## v1.4.2 (14/06/2018)
- Portability enhancements using Docker (issue #8).

## v1.4.1 (05/03/2018)
- Fixed bug in the analysis with delta=0 for the NNLR method (issue #6).

## v1.4 (22/01/2018)
- Code optimization.
- Fixed bugs for analysis including additional factors.
- Added gene dropping simulation for estimate ancestral inbreeding.
- Improved output options and functions.

## v1.3 (29/07/2017)
- Code optimization and small bugs fixes.
- Removed input files hard-coding.
- Added default settings and command line options.
- Higher performance of NNLR method (ABC convergence).
- Portability enhancements.

**note**: this is the version used in López-Cortegano et al (2018).

**known issues**
- Lower performance for LR method (golden section search algorithm removed).
- Analysis including additional factors may break.

## v1.2 (25/04/2017)
- It is the first public release.
