#include <iterator>
#include <random>
#include "../src/alias.h"
#include "../src/error.h"
#include "../src/templates.h"
#include "../src/operators.h"
#include "../src/pedigree.h"

sMatrix input_Smatrix (std::string, bool);
iMatrix read_pedigree ( const sMatrix& );
std::size_t sample_allele (const iMatrix&, std::size_t, std::size_t);
std::size_t sample_allele (iVector);
void search_ancestors (iMatrix, std::size_t, bVector&, bVector&);
void clean_parents (iMatrix&, std::size_t);
void rm_ancestor (iMatrix&, bVector&, std::size_t);

typedef std::uniform_real_distribution<coefficient> Unif;
Unif dist(0.0, 1.0);
std::mt19937 random_generator;
constexpr std::size_t col_id = 0;
constexpr std::size_t col_dam = 1;
constexpr std::size_t col_sir = 2;
constexpr std::size_t all_dam = 3;
constexpr std::size_t all_sir = 4;
const name RP="1";
constexpr std::size_t nboot = 10000;

/* =======================================================================================
 * pedigree_ancestors: 
 * found founders and ancestors of a pedigree, and calculate their effective numbers.
 * the number of founder genome equivalents is also computed.
 * Note: it requires to define which individuals belong to a reference population (RP)
 * See the README file
 *
 * TODO: optimize code?
 ======================================================================================= */

int main (int argc, char *argv[]) {

	if (argc != 2) {
		std::cerr << "ERROR: Invalid number of arguments. Check the README file." << std::endl;
		exit(-1);
	}
	//random_generator.seed(1236);

	name filename = argv[1];
	sMatrix pedigree = input_Smatrix(filename, true);

	for (std::size_t i(1); i<pedigree.size(); ++i) {
		if (pedigree[i][0] != std::to_string(i)) {
			std::cerr << "ERROR: Individuals must be named and sorted from 1 to N ("<< pedigree.size()-1 << ")." << std::endl;
			std::cerr << "Run: pedigree_rename " << filename << std::endl;
			exit(-1);
		}
	}

	// Check reference population
	name colname_ref ("reference");
	std::size_t col_ref (0);
	sVector header = pedigree[0];
	if (!is_in(colname_ref, header)) {
		std::cerr << "ERROR: No column indicating 'reference' population" << std::endl;
		exit(-1);
	} else {
		for (std::size_t i(0); i<header.size(); ++i) {
			if (header[i]==colname_ref) { col_ref = i; break;}
		}
	}

	// Given that individuals must be ordered form older to younger
	// the pedigree is filteret up to the last individual in the RP
	// (i.e. individuals younger than the youngest in the RP are discarded)
	std::size_t last (1);
	for (sMatrix::reverse_iterator it = pedigree.rbegin(); it != pedigree.rend(); ++it) {
		if ((*it)[col_ref]==RP) {
			last = std::distance(pedigree.begin(), it.base()) - 1;
			break;
		}
	}
	sMatrix tmp_ped;
	for (std::size_t i(1); i<=last; ++i) {
		tmp_ped.push_back(pedigree[i]);
	}
	pedigree = tmp_ped;
	tmp_ped.clear();
	iMatrix ipedigree = read_pedigree(pedigree);
	iMatrix original_ipedigree (ipedigree);

	// Search founders and ancestors (must be related to the RP)
	std::size_t N (pedigree.size());
	std::size_t Nrp (0);
	bVector founders (N, false);
	bVector ancestors (N, false);
	for (std::size_t i(last-1); i>0; --i) {
		if (pedigree[i][col_ref]==RP) {
			++Nrp;
			search_ancestors (ipedigree, i, founders, ancestors);
		}
	}
	if (Nrp==N) {
		std::cerr << "Error: The reference population cannot be the full population" << std::endl;
		exit(-1);
	} else if (!Nrp) {
		std::cerr << "Error: The reference population must have at least one individual" << std::endl;
		exit(-1);
	}

	// Expected contribution of ancestors (probability that a gene in the RP originates in founder i)
	Vector q (N, 0.0);
	for (std::size_t i(0); i<N; ++i) {
		if (pedigree[i][col_ref]==RP) q[i] = 1.0;
	}
	Vector q_init (q);
	for (iMatrix::reverse_iterator it = ipedigree.rbegin(); it != ipedigree.rend(); ++it) {
		iVector ind = *it;
		if (ind[col_sir]) q[ind[col_sir]-1] += 0.5*q[ind[col_id]-1];
		if (ind[col_dam]) q[ind[col_dam]-1] += 0.5*q[ind[col_id]-1];
	}
	for (std::size_t i(0); i<N; ++i) {
		if (ipedigree[i][col_dam] && !ipedigree[i][col_sir]) q[i] *= 0.5;
		else if (!ipedigree[i][col_dam] && ipedigree[i][col_sir]) q[i] *= 0.5;
	}
	q /= (coefficient)Nrp;

	// Number of founders and their relative contributions
	std::size_t nf (0);
	coefficient fe (0.0);
	Vector qf;
	coefficient sum_qf (0.0);
	for (std::size_t i(0); i<N; ++i) {
		if (founders[i]) {
			++nf;
			sum_qf += q[i];
		}
	}
	for (std::size_t i(0); i<N; ++i) {
		if (founders[i]) {
			qf.push_back(q[i]/coefficient(sum_qf));
		}
	}
	for (const auto& qi: qf) { fe += qi*qi; }
	fe = 1.0 / fe;

	// Number of ancestors and their relative (marginal) contributions
	bVector original_ancestors (ancestors);
	std::size_t na (0);
	coefficient fa (0.0);
	coefficient maxq (0.0);
	std::size_t major (N-1);
	for (std::size_t i(0); i<N; ++i) {
		if (ancestors[i]) {
			++na;
			if(q[i] > maxq) {
				maxq = q[i];
				major = i;
			}
		}
	}
	Vector pf (N, 0.0);
	bVector used_a (N, false);
	pf[major] = q[major];
	used_a[major] = true;
	rm_ancestor (ipedigree, ancestors, major);
	while (is_in(true, ancestors)) {
		// compute q
		Vector tmp_q = q_init;
		for (iMatrix::reverse_iterator it = ipedigree.rbegin(); it != ipedigree.rend(); ++it) {
			std::size_t i = std::distance(ipedigree.begin(), it.base()) - 1;
			if (used_a[i]) continue;
			iVector ind = *it;
			if (ind[col_sir]) tmp_q[ind[col_sir]-1] += 0.5*tmp_q[ind[col_id]-1];
			if (ind[col_dam]) tmp_q[ind[col_dam]-1] += 0.5*tmp_q[ind[col_id]-1];
		}
		// compute a
		Vector a (N, 1.0);
		for (std::size_t i(0); i<N; ++i) if (ancestors[i]) a[i] = 0.0;
		for (std::size_t i(0); i<N; ++i) {
			if (used_a[i]) continue;
			iVector ind (ipedigree[i]);
			if ((ind[col_sir]-1) && a[ind[col_sir]-1] > 0.0) a[ind[col_id]-1] += 0.5*a[ind[col_sir]-1];
			if ((ind[col_dam]-1) && a[ind[col_dam]-1] > 0.0) a[ind[col_id]-1] += 0.5*a[ind[col_dam]-1];
		}
		// compute p
		Vector p (tmp_q);
		for (std::size_t i(0); i<N; ++i) { p[i] *= (1.0-a[i]); }
		// choose new major
		coefficient maxp (-1.0);
		for (iMatrix::reverse_iterator it = ipedigree.rbegin(); it != ipedigree.rend(); ++it) {
			std::size_t i = std::distance(ipedigree.begin(), it.base()) - 1;
			if (!ancestors[i]) continue;
			if (p[i] > maxp) { maxp = p[i]; major = i; }
		}
		// final f
		pf[major] = p[major];
		pf[major] /= (coefficient)Nrp;
		// clean ped
		rm_ancestor(ipedigree, ancestors, major);
		used_a[major] = true;
	}
	for (std::size_t i(0); i<N; ++i) {
		if (original_ancestors[i]) fa += (pf[i]*pf[i]);
	}
	fa = 1.0 / fa;

	// Effective number of founder genomes
	ipedigree = original_ipedigree;
	iVector abs_freq;
	Vector v_Ng;
	for (std::size_t b(0); b<nboot; ++b) {
		// simulate segregation
		abs_freq.clear();
		//abs_freq = iVector(12,0);
		iMatrix tmp_ped (ipedigree);
		std::size_t al_count(0);
		for (std::size_t i(0); i<N; ++i) {
			if (!tmp_ped[i][col_dam] && !tmp_ped[i][col_sir]) {
				tmp_ped[i].push_back(al_count++);
				tmp_ped[i].push_back(al_count++);
				abs_freq.push_back(0);
				abs_freq.push_back(0);
			} else if (!tmp_ped[i][col_dam]) {
				abs_freq.push_back(0);
				if (pedigree[i][col_ref]==RP) ++abs_freq[abs_freq.size()-1];
				tmp_ped[i].push_back(al_count++);
				std::size_t al (sample_allele(tmp_ped, i, col_sir));
				if (pedigree[i][col_ref]==RP) ++abs_freq[al];
				tmp_ped[i].push_back(al);
			} else if (!tmp_ped[i][col_sir]) {
				std::size_t al (sample_allele(tmp_ped, i, col_dam));
				tmp_ped[i].push_back(al);
				if (pedigree[i][col_ref]==RP) ++abs_freq[al];
				abs_freq.push_back(0);
				if (pedigree[i][col_ref]==RP) ++abs_freq[abs_freq.size()-1];
				tmp_ped[i].push_back(al_count++);
			} else {
				std::size_t al1 (sample_allele(tmp_ped, i, col_dam));
				std::size_t al2 (sample_allele(tmp_ped, i, col_sir));
				tmp_ped[i].push_back(al1);
				tmp_ped[i].push_back(al2);
				if (pedigree[i][col_ref]==RP) {
					++abs_freq[al1];
					++abs_freq[al2];
				}
			}
		}
		Vector rel_freq;
		std::size_t n2f (abs_freq.size());
		for (std::size_t i(0); i<n2f; ++i) rel_freq.push_back((coefficient)abs_freq[i]/(2.0*Nrp));
		coefficient Ngi (0.0);
		for (std::size_t i(0); i<n2f; ++i) Ngi += (rel_freq[i]*rel_freq[i]);
		Ngi = 1.0 / (2.0*Ngi);
		v_Ng.push_back(Ngi);
	}
	coefficient Ng (0.0);
	for (const auto& i: v_Ng) Ng += i;
	Ng /= coefficient(nboot);

	// Results
	std::cout << "Results for pedigree: \"" << filename << "\" for a reference population of size: " << Nrp << std::endl;
	std::cout << "Total number of founders: " << nf << std::endl;
	std::cout << "Effective number of founders (fe): " << fe << std::endl;
	std::cout << "Total number of ancestors: " << na << std::endl;
	std::cout << "Effective number of ancestors (fa): " << fa << std::endl;
	std::cout << "Founder genome equivalents (Ng): " << Ng << std::endl;

	return 0;
}

std::size_t sample_allele (const iMatrix& ped, std::size_t ind, std::size_t par) {
	std::size_t al(0);
	coefficient random = dist(random_generator);
	if (random < 0.5) al = ped[ped[ind][par]-1][all_dam];
	else al = ped[ped[ind][par]-1][all_sir];
	return al;
}

std::size_t sample_allele (iVector alls) {
	return sample(alls);
}

void search_ancestors (iMatrix pedigree, std::size_t i, bVector& founders, bVector& ancestors) {
	std::size_t imom (pedigree[i][col_dam]);
	std::size_t isir (pedigree[i][col_sir]);
	if (!imom && !isir) {
		founders[i] = true;
		ancestors[i] = true;
		return;
	} else {
		if (imom && !ancestors[imom-1]) {
			ancestors[imom-1] = true;
			search_ancestors(pedigree, imom-1, founders, ancestors);
		}
		if (isir && !ancestors[isir-1]) {
			ancestors[isir-1] = true;
			search_ancestors(pedigree, isir-1, founders, ancestors);
		}
	}
}

iMatrix read_pedigree (const sMatrix& data) {
	iMatrix pedigree;
	for (size_t i(0), N(data.size()); i<N; ++i) {
		iVector row;
		row.push_back(atoi(data[i][icol].c_str()));
		row.push_back(atoi(data[i][dcol].c_str()));
		row.push_back(atoi(data[i][scol].c_str()));
		pedigree.push_back(row);
	}
	return pedigree;
}

void clean_parents (iMatrix& ped, std::size_t i) {
	ped[i][col_dam] = 0;
	ped[i][col_sir] = 0;
}

void rm_ancestor (iMatrix& ped, bVector& anc, std::size_t i) {
	clean_parents (ped, i);
	anc[i] = false;
}
