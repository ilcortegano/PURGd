#include <iterator>
#include <random>
#include <deque>
#include "../src/alias.h"
#include "../src/error.h"
#include "../src/templates.h"
#include "../src/operators.h"
#include "../src/pedigree.h"

/*sMatrix input_Smatrix (std::string, bool);
iMatrix read_pedigree ( const sMatrix& );
std::size_t sample_allele (const iMatrix&, std::size_t, std::size_t);
std::size_t sample_allele (iVector);
void search_ancestors (iMatrix, std::size_t, bVector&, bVector&);
void clean_parents (iMatrix&, std::size_t);
void rm_ancestor (iMatrix&, bVector&, std::size_t);*/

/* =======================================================================================
 * pedigree_ancestors: 
 * A utility to remove individuals that do not have fitness information (eg. w=NA) and that
 * do not contribute to descendants with fitness information.
 *
 ======================================================================================= */

int main (int argc, char *argv[]) {

	if (argc != 2) {
		std::cerr << "ERROR: Invalid number of arguments. Check the README file." << std::endl;
		exit(-1);
	}

	name filename = argv[1];
	sMatrix pedigree = input_Smatrix(filename, true);

	for (std::size_t i(1); i<pedigree.size(); ++i) {
		if (pedigree[i][0] != std::to_string(i)) {
			std::cerr << "ERROR: Individuals must be named and sorted from 1 to N ("<< pedigree.size()-1 << ")." << std::endl;
			std::cerr << "Run: pedigree_rename " << filename << std::endl;
			exit(-1);
		}
	}

	// Filtering
	// Last individuals with NA data are removed (they do not contribute to inbreeding of any individual with non-NA fitness)
	std::size_t N (pedigree.size());
	std::size_t last_idx (N-1);
	for (std::size_t i(last_idx); i>0; --i) {
		if (pedigree[i][wcol]!="NA") {
			last_idx = i;
			break;
		}
	}
	sMatrix f_pedigree;
	for (std::size_t i(0); i<last_idx+1; ++i) f_pedigree.push_back(pedigree[i]);

	// Remove other individuals with NA fitness and without descendant
	N = f_pedigree.size();
	//sVector na_idx;
	sVector rm_idx;
	sVector dams;
	sVector sirs;
	for (std::size_t i(N-1); i>0; --i) {
		sVector row (f_pedigree[i]);
		std::string id (row[icol]);
		if (row[wcol]=="NA" && ((!is_in(id,dams)) && (!is_in(id,sirs)))) rm_idx.push_back(id);
		dams.push_back(row[dcol]);
		sirs.push_back(row[scol]);
	}

	sMatrix f2_pedigree;
	for (std::size_t i(0); i<f_pedigree.size(); ++i) {
		if (!is_in(f_pedigree[i][0],rm_idx)) f2_pedigree.push_back(f_pedigree[i]);
	}

	// Output
	for (std::size_t i(0); i<f2_pedigree.size(); ++i) {
		for (std::size_t j(0); j<f2_pedigree[i].size(); ++j) {
			std::cout << f2_pedigree[i][j];
			if (j<f2_pedigree[i].size()-1) std::cout << ',';
		}	std::cout << std::endl;
	}

	return 0;
}
