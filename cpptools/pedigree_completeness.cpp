#include "../src/alias.h"
#include "../src/error.h"
#include "../src/templates.h"

sMatrix input_Smatrix (std::string, bool);

constexpr std::size_t col_i = 0;
constexpr std::size_t col_dam = 1;
constexpr std::size_t col_sir = 2;

/* =======================================================================================
 * pedigree_completeness: 
 * returns the degree of pedigree completeness: 100 * N inds with both parents known / N 
 ======================================================================================= */

int main (int argc, char *argv[]) {

	if (argc != 2) {
		std::cerr << "ERROR: Invalid number of arguments. Check the README file." << std::endl;
		exit(-1);
	}

	name filename = argv[1];
	sMatrix pedigree = input_Smatrix(filename, false);

	coefficient c(0.0);
	for (std::size_t i(0); i<pedigree.size(); ++i) {
		if (pedigree[i][col_dam]!="0" && \
			pedigree[i][col_sir]!="0" && \
			pedigree[i][col_dam]!="NA" && \
			pedigree[i][col_sir]!="NA") c += 1.0;
	}

	std::cout << "Number of individuals with both parents known = " << c << std::endl;
	std::cout << "Total number of individuals: " << pedigree.size() << std::endl;
	std::cout << "Pedigree completeness is: " << 100.0*c/pedigree.size() << std::endl;

	return 0;
}
