##   PURGd INPUT README

The software PURGd is distributed with 20 example pedigree files:

```
d1.csv - d10.csv
msd1.csv - msd10.csv
```
You can start running PURGd using these files. They will work the same in GNU/Linux and Windows environments.

However, we warn that these files have been written in a UNIX machine, so they are LF ended and may be hard to visualize in Windows (CR+LF ended). If opened with Excel in Windows, 
they can easily be converted for a friendly view (select the first column in the file, go to DATA, and choose "text in columns" > "delimited" > "comma").

These files are a subset of those analyzed in the publication introducing this software (García-Dorado *et al*. 2016), corresponding to small lines derived from a large base population, where completely recessive mutations (h=0) with homozygous effect s=0.3 continuously occur at a rate of 0.1 per gamete and generation. 

In the first set of pedigree files (msd) natural selection operated both in the base population and in the small lines, so that purging is expected to occur. In the second one (d) natural selection was relaxed in the small lines, so that no purging is expected.
