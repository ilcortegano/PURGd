# Makefile for compile PURGd

############################################################
# VARIABLES
############################################################

EXE      = PURGd
INSTALL ?= /usr/local
CC       = g++
VPATH    = src
DEBUG    = -pg -ggdb
CPPFLAGS = -O3 -ansi -pedantic -Wall -Wextra -Wdouble-promotion -std=c++14
LDFLAGS  = -flto -static
LIBS     = -lm
SOURCES := $(wildcard src/*.cpp)
H_LIBS   = alias.h error.h operators.h templates.h
HEADERS  = $(SOURCES:.cpp=.h) $(H_LIBS)
OBJECTS  = $(SOURCES:.cpp=.o)

############################################################
# RULES
############################################################

all: $(EXE) $(SOURCES) $(HEADERS) $(OBJECTS)

$(EXE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@ $(LIBS)

$(OBJECTS): %.o: %.cpp %.h $(H_LIBS)
	$(CC) -c $(CPPFLAGS) $< -o $@

.PHONY: install
install:
	mkdir -p $(INSTALL)/bin
	install -m 557 $(EXE) $(INSTALL)/bin
	rm $(EXE)

.PHONY: clean
clean:
	rm -f $(EXE)
	rm -f src/*.o
	rm -f src/*.gch
