FROM alpine:3.4

LABEL version="2.2"
LABEL description="A software to detect purging and to estimate inbreeding-purging genetic parameters in pedigreed populations."
LABEL maintainer="e.lopez-cortegano@ed.ac.uk"

COPY src ./src
COPY Makefile .
RUN apk update && apk add \
    g++ \
    make \
    && ls -l \
    && make clean && make && make install \
    && apk del g++ make \
    && rm -rf ./src

WORKDIR /tmp

ENTRYPOINT ["PURGd"]
